# Hematology_Server

- After taking a fresh pull from git delete files if exists.
    - `node_modules`
    - `package-lock.json`
- Environment module with configurations is loaded into `index.js`
- Update credentials(smtp,s3 & other)

### MYSQL CONFIGURATION
1. Load environment configuration in `env.js`
2. Define configuration in `app/utils/services/mysql.js`

```
const { sql } = require('./../../../../utils').services
sql.query("SELECT * FROM TABLE WHERE 1 = ?", [1], returnSingleObject:Boolean, (error,response))
```
reference  [mysql](https://www.npmjs.com/package/mysql)

    

### MONGODB CONFIGURATION
1.Load environment configuration in `env.js`
2.Define configuration in `app.js`

### API & FLOW NOTES 
1. User will be remove from DB when mail sent fail.Update if you don't want to do that action in registration
2. User model consist some functions which can be reusable look in to it 
3. You can restrict user login by "process.env.LOGIN_LIMIT".Set '0' for no restriction
4. Admin loginLimit is separate & change_pw,logout use userControllers service