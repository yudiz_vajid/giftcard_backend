require('module-alias/register')
require('./env')

const num_processes = 1 // require('os').cpus().length
const farmhash = require('farmhash')
const cluster = require('cluster')
const net = require('net')

if (cluster.isMaster) {
    let workers = []
    for (var i = 0; i < num_processes; i++) spawn(i)
    net.createServer({
        pauseOnConnect: true
    }, (connection) => {
        let worker_index = farmhash.fingerprint32(connection.remoteAddress) % num_processes
        let worker = workers[worker_index]
        worker.send('sticky-session:connection', connection)
    }).listen(process.env.PORT, () => console.log(`Listining on ${process.env.PORT}`))

    function spawn(i) {
        workers[i] = cluster.fork()
        workers[i].on('exit', (code, signal) => spawn(i))
    }
} else {
    require('./app')
}