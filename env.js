//GENERAL
process.env.BASE_URL = "http://192.168.11.118"
process.env.PORT = 3008
process.env.PROD = false
process.env.MAIL_HOST_LINK = 'http://192.168.11.118:3008/api/v1'
process.env.JWT_MAIL_VALIDITY = "24h"
process.env.SITE_NAME = 'GiftCard'
process.env.SITE_LOGO = 'http://192.168.11.118:3008/public/logo/user1.png'
process.env.siteMediaUploadPath = 'local'
process.env.LOGIN_LIMIT = 0
process.env.ADMIN_LOGIN_LIMIT = 0
process.env.FRONT_URL = "http://192.168.11.118:3000"
// process.env.FRONT_URL = "http://192.168.11.115:4200"

//SETTINGS
process.env.UPLOAD_PATH = './public/uploads'
process.env.profilePicturePath = './public/uploads/profilePictures'
process.env.categoryImagePath = './public/uploads/categoryImages'
process.env.categoryPdfPath = './public/uploads/categoryPdf'
process.env.sponsorImageCompPath = './public/uploads/sponsorImage'
process.env.sponsorImagePath = './public/uploads/sponsorComImage'
process.env.VERIFICATION_CODE_LENGTH = 4
process.env.JWT_SECRET = 12211221
process.env.JWTPASSPHERE = "secret123"
process.env.EMAIL_TEMPLATE_PATH = 'app/utils/email_templates/'
process.env.sponsorImageURL = '/uploads/sponsorImage/'

// MYSQL 
process.env.MYSQL_HOST = '127.0.0.1'
process.env.MYSQL_USER = 'username'
process.env.MYSQL_PASSWORD = 'password'
process.env.MYSQL_PORT = 3306
process.env.MYSQL_DATABASE = 'dbname'

//AWS SES
process.env.AWS_REGION = 'eu-west-1'
process.env.AWS_ACCESSKEYID = "ACCESSKEY"
process.env.AWS_SECRETKEY = "SECRETKEY"

//TWILIO
process.env.TWILIO_ACCOUNTSID = "*********sadasd13464ADSADWS"
process.env.TWILIO_SECRET = "*********asADSWD45578"

//NEXMO
process.env.NEXMO_APIKEY = "***************"
process.env.NEXMO_APISECRET = "***************"

// MONGOOSE
process.env.DB_URL = "mongodb://localhost:27017/GiftCard"

// SMTP
process.env.SMTP_FROM = 'GiftCard'
process.env.SMTP_USER_EMAIL = 'yudizmean@gmail.com'
process.env.SMTP_PASS = 'Yudiz@321'

// AWS
process.env.awsAccesskeyID = 'Your aws awsAccesskeyID'
process.env.awsSecretAccessKey = 'Your aws awsSecretAccessKey'
process.env.awsS3Bucket = 'Your awsS3Bucket'


process.env.CARD_IMAGE_PATH = './public/uploads/cardImages'
