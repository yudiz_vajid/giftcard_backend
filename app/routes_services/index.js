const bodyParser = require('body-parser')
const helmet = require('helmet')
const cors = require('cors')
const expressDevice = require('express-device')
const expressValidator = require('express-validator');
const path = require('path');
const morgan = require('morgan');
const fs = require('fs');


const usercontroller = require('@userLib/controllers')

module.exports = (app) => {
    /**
     *  Dependency Initialization
     */
    const logFile = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' });
    app.use(morgan(':remote-addr [:date[web]] :method :url :status - :response-time ms', { stream: logFile }));
    app.use(cors())
    app.use(helmet())
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json())
    app.use(expressDevice.capture())
    app.use((req, res, next) => {
        res.header('Access-Control-Expose-Headers', 'Authorization')
        next()
    });
    app.use(require('express').static('./public'))
    app.use(expressValidator());
    app.set('view engine', 'ejs');
    /**
     *  Routes definations
     */
    // app.get('/api/v1/reset/:sVerificationToken', usercontroller.reset);

    app.use('/api/v1/user/', require('./user'))
    app.use('/api/v1/admin/', require('./admin'))

}