const AuthRouter = require('express').Router()

const validator = require('./lib/validators')
const middlewares = require('./lib/middlewares')
const controllers = require('./lib/controllers')
const cmsService = require('./lib/services/cms.service')
const faqService = require('./lib/services/faq.service')

// USER AUTH
AuthRouter.post('/register', validator.registration, controllers.registration)
AuthRouter.post('/login', validator.login, controllers.userLogin)
AuthRouter.post('/login/social', validator.socialLogin, controllers.socialLogin)
AuthRouter.get('/logout', middlewares.isUserAuthenticated, controllers.userLogout)
AuthRouter.get('/mail/verification/:sVerificationToken', controllers.verifyuserEmail);
AuthRouter.post('/forgot/password/mail', validator.forgotPasswordmail, controllers.userForgotPasswordMail)
AuthRouter.post('/reset/password', validator.forgotPassword, controllers.userForgotPassword);
AuthRouter.post('/password/change', validator.changePassword, middlewares.isUserAuthenticated, controllers.userChangePassword);
// USER PROFILE
AuthRouter.get('/profile', middlewares.isUserAuthenticated, controllers.getProfile)
AuthRouter.put('/profile', validator.updateProfile, middlewares.isUserAuthenticated, controllers.updateProfile)
AuthRouter.post('/profilepicture', middlewares.isUserAuthenticated, controllers.userProfilePictureUpload);

/**
 * Contact_us / User messages
 */
AuthRouter.post('/contactus', validator.contactUs, controllers.contactUsCreate)

/**
 * Question service
 */
AuthRouter.get('/faq', faqService.list)


// Cms Management
AuthRouter.get('/aboutus/cms', cmsService.viewAboutUs)
AuthRouter.get('/tc/cms', cmsService.viewTermsCondition)
AuthRouter.get('/privacypolicy/cms', cmsService.viewPrivacyPolicy)
AuthRouter.get('/cms/:iItemId', cmsService.deatailCms)
AuthRouter.get('/list/cms', cmsService.list)

module.exports = AuthRouter