const { messages, endpoints } = require('@utils');
const ObjectId = require('mongodb').ObjectID;
const debug = require('debug')('http')
// Dynamic model dep.
const usersModel = require('@models/users.model');
const cmsModel = require('@models/cms.model');


const addItem = (model, unique_field, itemName, body, req, res) => {
    try {
        if (unique_field && unique_field != '') body[unique_field].trim();
        model.findOne({ [unique_field]: body[unique_field] }).then((item_found) => {
            if (item_found) {
                return endpoints.send(res, messages.already_exists(itemName))
            } else {
                let newItem = new model(body)
                newItem.save().then((item) => {
                    return endpoints.send(res, messages.created(itemName), item)
                }).catch((error) => {
                    debug('error :: ', error)
                    return endpoints.send(res, messages.custom('DATA_SAVE_ERR'))
                })
            }
        }).catch((error) => {
            debug('error :: ', error)
            return endpoints.send(res, messages.custom('FETCH_ERR'))
        })
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

const listItem = (model, itemName, req, res) => {
    try {
        model.find({ bStatus: true }).then((item) => {
            if (!item) return endpoints.send(res, messages.not_found('item'))
            return endpoints.send(res, messages.list_succ(itemName), item)
        }).catch((error) => {
            debug('error :: ', error)
            return endpoints.send(res, messages.custom('FETCH_ERR'))
        })
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

const viewItem = (model, itemName, req, res) => {
    try {
        model.findById(ObjectId(req.params.iItemId)).then((item) => {
            if (!item) return endpoints.send(res, messages.not_found(itemName))
            return endpoints.send(res, messages.get_succ(itemName), item)
        }).catch((error) => {
            debug('error :: ', error)
            return endpoints.send(res, messages.custom('FETCH_ERR'))
        })
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}


const editItem = (model, unique_field, itemName, body, req, res) => {
    try {
        model.findById(ObjectId(req.params.iItemId)).then((itemFound) => {
            if (!itemFound) return endpoints.send(res, messages.not_found(itemName));
            if (body[unique_field]) {
                body[unique_field].trim()
                model.findOne({ [unique_field]: body[unique_field], _id: { $ne: ObjectId(req.params.iItemId) } }).then((Found) => {
                    if (Found) return endpoints.send(res, messages.already_exists(unique_field.substring(1)));
                    updateItem(body);
                }).catch((error) => {
                    debug('error :: ', error)
                    return endpoints.send(res, messages.custom('FETCH_ERR'))
                })
            } else {
                updateItem(body);
            }
        }).catch((error) => {
            debug('error :: ', error)
            return endpoints.send(res, messages.custom('FETCH_ERR'))
        })

        function updateItem(body) {
            model.findByIdAndUpdate(ObjectId(req.params.iItemId), { $set: body }, { new: true }).then((updatedItem) => {
                if (!updateItem) return endpoints.send(res, messages.not_found(itemName));
                return endpoints.send(res, messages.updated_succ(itemName), updatedItem)
            }).catch((error) => {
                debug('error :: ', error)
                return endpoints.send(res, messages.custom('UPDATE_ERR'))
            })
        }
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

// Hard delete
const deleteItem = (model, itemName, req, res) => {
    try {
        model.findByIdAndRemove(ObjectId(req.params.iItemId)).then((item) => {
            if (!item) return endpoints.send(res, messages.not_found(itemName))
            return endpoints.send(res, messages.delete_succ(itemName), item)
        }).catch((error) => {
            debug('error :: ', error)
            return endpoints.send(res, messages.custom('REMOVE_ERR'))
        })
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}


const softDeleteItem = (model, itemName, req, res) => {
    try {
        model.findByIdAndUpdate(ObjectId(req.params.iItemId), { $set: { bUserStatus: false } }, { new: true }).then((item) => {
            if (!item) return endpoints.send(res, messages.not_found(itemName))
            return endpoints.send(res, messages.delete_succ(itemName), item)
        }).catch((error) => {
            debug('error :: ', error)
            return endpoints.send(res, messages.custom('REMOVE_ERR'))
        })
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

const changeItemStatus = (model, itemName, req, res) => {
    try {
        let body = {
            bStatus: req.body.bStatus
        }
        model.findByIdAndUpdate(ObjectId(req.body.iItemId), { $set: body }, { new: true }).then((item) => {
            if (!item) return endpoints.send(res, messages.not_found(itemName))
            return endpoints.send(res, messages.updated_succ(itemName), item)
        }).catch((error) => {
            debug('error :: ', error)
            return endpoints.send(res, messages.custom('UPDATE_ERR'))
        })
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}


const listDynamicItem = (model, itemName, req, res, next) => {
    try {
        if (!req.body.start) {
            req.body.start = 0
        }
        if (!req.body.search) {
            req.body.search = ''
        }

        let sort;
        if (req.body.sortby && req.body.sortby != '') {
            let stype = req.body.sortby;
            let sdir = (req.body.orderby === 'desc') ? -1 : 1;
            sort = {
                [stype]: sdir
            }
        } else {
            sort = {
                'dCreatedAt': -1
            }
        }

        var and;
        if (!req.body.bStatus || req.body.bStatus == 'All' || req.body.bStatus == '') {
            and = [{
                bStatus: true
            }]
        }
        if (req.body.bStatus == 'true') {
            and = [{
                bStatus: true
            }]
        }
        if (req.body.bStatus == 'false') {
            and = [{
                bStatus: false
            }]
        }

        let query = {
            $and: and,
            $or: [
                {
                    sTitle: {
                        $regex: '^' + req.body.search,
                        $options: 'i'
                    }
                },
                {
                    sPageName: {
                        $regex: '^' + req.body.search,
                        $options: 'i'
                    }
                },
                {
                    sName: {
                        $regex: '^' + req.body.search,
                        $options: 'i'
                    }
                }
            ]
        }


        let skip = parseInt(req.body.start)
        let aggrigationQuery = [{
            $facet: {
                "data": [{
                    $match: query
                },
                {
                    $sort: sort
                },
                {
                    $skip: skip
                },

                {
                    $limit: parseInt(req.body.size)
                }
                ],
                "count": [{
                    $match: query
                }, {
                    $count: "totalData"
                }]
            }
        }]
        model.aggregate(aggrigationQuery, (error, docs) => {
            if (error) return endpoints.send(res, messages.custom('FETCH_ERR'))
            let result = {
                data: docs[0].data,
                count: docs[0].count[0]
            }
            if (docs[0].data.length == 0) {
                result = {
                    data: [],
                    count: {
                        "totalData": 0
                    }
                }
            }
            // if (docs.length) return endpoints.send(res, messages.list_succ(itemName), result)
            return endpoints.send(res, messages.list_succ(itemName), result)
        });
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

const listDynamicItemLatest = (model, itemName, req, res, next) => {
    try {
        if (!req.body.start) {
            req.body.start = 0
        }
        let search = ''
        if (req.body.search && req.body.search != '') {
            search = req.body.search
                .replace(/\\/g, '\\\\')
                .replace(/\$/g, '\\$')
                .replace(/\*/g, '\\*')
                .replace(/\)/g, '\\)')
                .replace(/\(/g, '\\(')
                .replace(/'/g, "\\'")
                .replace(/"/g, '\\"').trim();
        }

        let sort;
        if (req.body.sortby && req.body.sortby != '') {
            let stype = req.body.sortby;
            let sdir = (req.body.orderby === 'desc') ? -1 : 1;
            sort = {
                [stype]: sdir
            }
        } else {
            sort = {
                'dCreatedAt': -1
            }
        }

        var and;
        if (!req.body.bStatus || req.body.bStatus == 'All' || req.body.bStatus == '') {
            and = [{
                bStatus: true
            }]
        }

        let query = {
            $and: and,
            $or: [
                {
                    sTitle: { $regex: new RegExp('^.*' + search + '.*', 'i') }
                },
                {
                    sPageName: { $regex: new RegExp('^.*' + search + '.*', 'i') }
                },
                {
                    sName: { $regex: new RegExp('^.*' + search + '.*', 'i') }
                }
            ]
        }


        let aggrigationQuery = [{
            $facet: {
                "data": [{
                    $match: query
                },
                {
                    $sort: sort
                },
                {
                    $skip: parseInt(req.body.start)
                },

                {
                    $limit: parseInt(req.body.size)
                }
                ],
                "count": [{
                    $match: query
                }, {
                    $count: "totalData"
                }]
            }
        }]

        model.aggregate(aggrigationQuery, (error, docs) => {
            if (error) return endpoints.send(res, messages.custom('FETCH_ERR'))
            if (docs[0].data.length <= 0) docs[0].count[0] = { "totalData": 0 }
            // docs[0].data.map(singleItem => {
            //     if (singleItem.sSponsorImage && singleItem.sSponsorImage != '') {
            //         singleItem.sSponsorImage = `${process.env.BASE_URL}:${process.env.PORT}${process.env.sponsorImageURL}${singleItem.sSponsorImage}`
            //     }
            // })
            let result = {
                data: docs[0].data,
                count: docs[0].count[0]
            }
            return endpoints.send(res, messages.list_succ(itemName), result)
        });
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

module.exports = {
    addItem,
    listItem,
    changeItemStatus,
    viewItem,
    editItem,
    deleteItem,
    listDynamicItem,
    softDeleteItem,
    listDynamicItemLatest
}
