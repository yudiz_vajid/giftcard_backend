const cmsModel = require('@models/cms.model')
const commonService = require('./common.service');
const { messages, endpoints } = require('@utils');
const ObjectId = require('mongodb').ObjectID;
const Cms_model_name = 'Cms'
const fs = require('fs');
const distanceGoogle = require('google-distance-matrix');
var geodist = require('geodist')
const axios = require('axios')
const crypto = require('crypto');



const list = (req, res, next) => {
    commonService.listItem(cmsModel, Cms_model_name, req, res);
}

const deatailCms = (req, res) => {
    commonService.viewItem(cmsModel, Cms_model_name, req, res);
}

const viewAboutUs = (req, res) => {
    let cms_is = ObjectId("5ceb6ad2b1c27504a2724052")
    cmsModel.findById(cms_is).then((item) => {
        if (!item) return endpoints.send(res, messages.not_found(Cms_model_name))
        return endpoints.send(res, messages.get_succ(Cms_model_name), item)
    }).catch((error) => {
        console.log('error :: ', error);
        return endpoints.send(res, messages.custom('FETCH_ERR'))
    })
}

const viewTermsCondition = (req, res) => {
    let cms_is = ObjectId("5ceb6abeb1c27504a2724051")
    cmsModel.findById(cms_is).then((item) => {
        if (!item) return endpoints.send(res, messages.not_found(Cms_model_name))
        return endpoints.send(res, messages.get_succ(Cms_model_name), item)
    }).catch((error) => {
        console.log('error :: ', error);
        return endpoints.send(res, messages.custom('FETCH_ERR'))
    })
}

const viewPrivacyPolicy = (req, res) => {
    let cms_is = ObjectId("5ceb6e855e23410508b492da")
    cmsModel.findById(cms_is).then((item) => {
        if (!item) return endpoints.send(res, messages.not_found(Cms_model_name))
        return endpoints.send(res, messages.get_succ(Cms_model_name), item)
    }).catch((error) => {
        console.log('error :: ', error);
        return endpoints.send(res, messages.custom('FETCH_ERR'))
    })
}



module.exports = {
    deatailCms,
    list,
    viewAboutUs,
    viewTermsCondition,
    viewPrivacyPolicy
}
