const faqModel = require('@models/question.model')
const commonService = require('./common.service');
const question_model_name = 'Question'


const list = (req, res, next) => {
    commonService.listItem(faqModel, question_model_name, req, res);
}


module.exports = {
    list
}