const { endpoints, messages } = require('@utils')
const { _ } = require('@utils/lib/helpers');
const validators = {}

const validateEmail = function (email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
validators.registration = async (req, res, next) => {
    const body = _.pick(req.body, ['sEmail', 'sPassword', 'sFirstName', 'sLastName', 'sCountry', 'sCurrency'])
    if (!body.sEmail) return endpoints.send(res, messages.required_field('email'), {})
    var mailValid = await validateEmail(body.sEmail)
    if (!mailValid) return endpoints.send(res, messages.incorrect('email'));
    if (!body.sPassword) return endpoints.send(res, messages.required_field('password'), {})
    if (!body.sFirstName) return endpoints.send(res, messages.required_field('FirstName'), {})
    if (!body.sLastName) return endpoints.send(res, messages.required_field('LastName'), {})
    if (!body.sCountry) return endpoints.send(res, messages.required_field('Country'), {})
    if (!body.sCurrency) return endpoints.send(res, messages.required_field('Currency'), {})
    return next()
}

validators.login = async (req, res, next) => {
    const body = _.pick(req.body, ['sEmail', 'sPassword'])
    if (!body.sEmail) return endpoints.send(res, messages.required_field('email'), {})
    var mailValid = await validateEmail(body.sEmail)
    if (!mailValid) return endpoints.send(res, messages.incorrect('email'));
    if (!body.sPassword) return endpoints.send(res, messages.required_field('password'), {})
    return next()
}

validators.updateProfile = async (req, res, next) => {
    const body = _.pick(req.body, ['sFirstName', 'sLastName'])
    if (!body.sFirstName) return endpoints.send(res, messages.required_field('firstName'), {})
    if (!body.sLastName) return endpoints.send(res, messages.required_field('lastName'), {})
    return next()
}

validators.forgotPasswordmail = async (req, res, next) => {
    const body = _.pick(req.body, ['sEmail'])
    if (!body.sEmail) return endpoints.send(res, messages.required_field('email'));
    var mailValid = await validateEmail(body.sEmail)
    if (!mailValid) return endpoints.send(res, messages.incorrect('email'));
    return next()
}

validators.forgotPassword = async (req, res, next) => {
    const body = _.pick(req.body, ['sNewPassword', 'sNewRetypedPassword', 'sVerificationToken'])
    if (!body.sNewPassword) return endpoints.send(res, messages.required_field('sNewPassword'));
    if (!body.sNewRetypedPassword) return endpoints.send(res, messages.required_field('sNewRetypedPassword'));
    if (!body.sVerificationToken) return endpoints.send(res, messages.required_field('sVerificationToken'));
    return next()
}
validators.changePassword = async (req, res, next) => {
    const body = _.pick(req.body, ['sOldPassword', 'sNewPassword', 'sNewRetypedPassword'])
    if (!body.sOldPassword) return endpoints.send(res, messages.required_field('sOldPassword'));
    if (!body.sNewPassword) return endpoints.send(res, messages.required_field('sNewPassword'));
    if (!body.sNewRetypedPassword) return endpoints.send(res, messages.required_field('sNewRetypedPassword'));
    if (req.body.sNewPassword !== req.body.sNewRetypedPassword) return endpoints.send(res, messages.not_matched('Password'))
    if (req.body.sOldPassword == req.body.sNewRetypedPassword) return endpoints.send(res, messages.custom('OLD_NEW_PASS_SAME'))
    return next()
}
validators.list = async (req, res, next) => {
    const body = _.pick(req.body, ['size'])
    if (!body.size) return endpoints.send(res, messages.required_field('size'), {})
    return next()
}
validators.socialLogin = async (req, res, next) => {
    const body = _.pick(req.body, ['sSocialToken'])
    if (!body.sSocialToken) return endpoints.send(res, messages.required_field('Google Token'), {})
    return next()
}

validators.contactUs = async (req, res, next) => {
    const body = _.pick(req.body, ['sFullName', 'sEmail', 'sSubject', 'sMessage'])
    if (!body.sFullName) return endpoints.send(res, messages.required_field('FullName'), {})
    if (!body.sEmail) return endpoints.send(res, messages.required_field('Email'), {})
    if (!body.sSubject) return endpoints.send(res, messages.required_field('Subject'), {})
    if (!body.sMessage) return endpoints.send(res, messages.required_field('Message'), {})
    return next()
}


module.exports = validators