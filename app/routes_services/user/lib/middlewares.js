// const services = require('./services')
const middlewares = {}
const { messages, endpoints } = require('@utils')
const usersModel = require('@models/users.model')
const customerModel = require('@models/customer.model');
const debug = require('debug')('http')
middlewares.registration = (req, res, next) => {
    next()
}

middlewares.isUserAuthenticated = (req, res, next) => {
    try {
        let token = req.header('Authorization');
        if (token) {
            customerModel.findByToken(token).then((user) => {
                if (!user) return endpoints.send(res, messages.unauthorised())
                req._id = user._id
                req.user = user;
                return next(null, null);
            }).catch(error => {
                debug('error :: ', error)
                return endpoints.send(res, messages.unauthorised())
            });
        } else {
            return endpoints.send(res, messages.unauthorised())
        }
    } catch (error) {
        debug('error :: ', error)
        console.log("error :: ", error);

        return endpoints.send(res, messages.server_error())
    }
}

module.exports = middlewares