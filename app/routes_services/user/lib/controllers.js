// const services = require('./services')
const { messages, endpoints } = require('@utils')
const { _ } = require('@utils/lib/helpers');
const controllers = {}
const usersModel = require('@models/users.model')
const customerModel = require('@models/customer.model');
const contactusModel = require('@models/contactus.model');
var ObjectId = require('mongodb').ObjectID;
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const { sendmail } = require('@utils/lib/services/nodemailer');
const mime = require('mime');
const AWS = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');
const debug = require('debug')('http')


var mediaUploadPath = process.env.siteMediaUploadPath;
AWS.config.update({ accessKeyId: process.env.awsAccesskeyID, secretAccessKey: process.env.awsSecretAccessKey });

var s3 = new AWS.S3();
if (mediaUploadPath == 's3') {
    // User Profile image upload on live domain - s3 Bucket
    var storage = multerS3({
        s3: s3,
        bucket: process.env.awsS3Bucket + '/profilePictures',
        acl: 'public-read',
        key: function (req, file, callback) {
            var fileName = `${req._id}_${Date.now()}`;
            callback(null, fileName + '.' + mime.getExtension(file.mimetype));
        }
    });
} else {
    // User Profile image upload on local domain
    var storage = multer.diskStorage({
        destination: function (req, file, callback) {
            callback(null, process.env.profilePicturePath);
        },
        filename: function (req, file, callback) {
            var fileName = `${req._id}_${Date.now()}`;
            callback(null, fileName + '.' + mime.getExtension(file.mimetype));
        }
    });
}

var uploadProfile = multer({
    storage: storage
}).fields([{
    name: 'sProfilePicture',
    maxCount: 1
}]);


const generateAuthToken = function (user) {
    let obj = {
        sToken: jwt.sign({
            _id: (user._id).toHexString()
        }, process.env.JWTPASSPHERE)
    };
    if (user.aJwtTokens.length < process.env.LOGIN_LIMIT || process.env.LOGIN_LIMIT == 0) {
        user.aJwtTokens.push(obj);
    } else {
        user.aJwtTokens.splice(0, 1);
        user.aJwtTokens.push(obj);
    }
    // user.aJwtTokens.push(obj);
    return user.save().then(function () {
        return obj.sToken;
    }).catch((error) => {
        debug('error :: ', error)
        return endpoints.send(res, messages.custom('DATA_SAVE_ERR'));
    });
}

/**
 * NOTE :- Send mail using nodemailer,if mail fail than user will be removed from DB
 * 
 */
controllers.registration = (req, res) => {
    try {
        let body = _.pick(req.body, ['sEmail', 'sPassword', 'sFirstName', 'sLastName', 'sPhoneNumber', 'sCurrency', 'sCountry'])
        if (body.sPhoneNumber) {
            var query = {
                $or: [{ sEmail: body.sEmail }, { sPhoneNumber: body.sPhoneNumber }]
            }
        } else {
            var query = {
                sEmail: body.sEmail
            }
        }
        customerModel.findOne(query).then((userFound) => {
            if (userFound) {
                if (body.sPhoneNumber && body.sPhoneNumber === userFound.sPhoneNumber) return endpoints.send(res, messages.already_exists('PhoneNumber'))
                if (body.sEmail && body.sEmail === userFound.sEmail) return endpoints.send(res, messages.already_exists('Email'))
            } else {
                var newUser = new customerModel(body)
                newUser.save().then((user) => {
                    user.generateAuthToken(req).then((userToken) => {
                        if (!userToken) return endpoints.send(res, messages.server_error())
                        user.sVerificationToken = usersModel.createVerificationToken(user._id) // jwt.sign({ _id: user._id }, process.env.JWT_SECRET, { expiresIn: process.env.JWT_MAIL_VALIDITY });

                        user.save().then((data) => {
                            sendmail('account_active.html',
                                {
                                    USERNAME: data.sFirstName,
                                    SITE_NAME: process.env.SITE_NAME,
                                    YEAR: new Date().getFullYear(),
                                    SITE_LOGO: process.env.SITE_LOGO,
                                    ACTIVELINK: `${process.env.MAIL_HOST_LINK}/user/mail/verification/${data.sVerificationToken}`
                                }, {
                                    from: process.env.SMTP_FROM,
                                    to: data.sEmail,
                                    subject: "Email Verification",
                                }).then(() => {
                                    return endpoints.send(res, messages.custom('reg_succ'))
                                }).catch(error => {
                                    debug('error :: ', error);
                                    // Remove user from DB due to email is not sent
                                    data.remove().then(() => {
                                        return endpoints.send(res, messages.custom('MAIL_SENT_FAIL'))
                                    })
                                });
                        }).catch(error => {
                            debug('error :: ', error)
                            return endpoints.send(res, messages.custom('DATA_SAVE_ERR'))
                        });
                    }).catch((error) => {
                        debug('error :: ', error)
                        return endpoints.send(res, messages.server_error())
                    })
                }).catch((error) => {
                    debug('error :: ', error)
                    return endpoints.send(res, messages.custom('DATA_SAVE_ERR'))
                })
            }
        }).catch((error) => {
            debug('error :: ', error)
            return endpoints.send(res, messages.server_error())
        })
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

controllers.userLogin = (req, res) => {
    try {
        let body = _.pick(req.body, ['sEmail', 'sPassword'])
        customerModel.findOne({ sEmail: body.sEmail, bUserStatus: true }).then((user) => {
            if (!user) return endpoints.send(res, messages.custom('LOGIN_FAIL'))
            if (bcrypt.compareSync(body.sPassword, user.sPassword)) {

                if (user.eIsActive === 'n') return endpoints.send(res, messages.custom('VERIFY_EMAIL'))
                if (user.eIsActive === 'b') return endpoints.send(res, messages.custom('USER_BLOCKED'))
                customerModel.findById({ _id: ObjectId(user._id) }).then((updated_user) => {
                    generateAuthToken(updated_user).then((token) => {
                        if (!token) return endpoints.send(res, messages.server_error())
                        let AuthorizationData = {
                            Authorization: token
                        }
                        return endpoints.send(res, messages.custom('LOGIN_SUCC'), AuthorizationData, { 'Authorization': token })
                    }).catch((error) => {
                        debug('error :: ', error)
                        return endpoints.send(res, messages.server_error())
                    })
                });
            } else {
                return endpoints.send(res, messages.custom('LOGIN_FAIL'))
            }
        }).catch((error) => {
            debug('error :: ', error)
            return endpoints.send(res, messages.custom('FETCH_ERR'))
        })
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

controllers.socialLogin = async (req, res) => {
    try {
        let userFound = await customerModel.findOne({ sGoogleId: req.body.sSocialToken })
        if (userFound) {
            let tokenPush = {
                sToken: jwt.sign({ _id: userFound._id.toHexString() }, process.env.JWTPASSPHERE),
                sPushToken: req.body.sPushToken,
                sIpAddres: req.connection.remoteAddress
            }
            userFound.aJwtTokens.push(tokenPush)
            userFound.save().then(data => {
                customerModel.filterUserData(data)
                let AuthorizationData = {
                    Authorization: tokenPush.sToken
                }
                return endpoints.send(res, messages.custom('LOGIN_SUCC'), AuthorizationData, { 'Authorization': tokenPush.sToken })
            }).catch(error => {
                debug("error :: ", error);
                return endpoints.send(res, messages.server_error())
            })
        } else {
            if (!req.body.sFirstName) return endpoints.send(res, messages.required_field('FirstName'), {})
            if (!req.body.sLastName) return endpoints.send(res, messages.required_field('LastName'), {})
            if (!req.body.sEmail) return endpoints.send(res, messages.required_field('email'), {})
            let newGoogleUser = new customerModel({
                sFirstName: req.body.sFirstName,
                sLastName: req.body.sLastName,
                sEmail: req.body.sEmail,
                sGoogleId: req.body.sSocialToken,
                eIsActive: 'y',
                dCreatedAt: Date.now()
            })
            newGoogleUser.save().then(savedUser => {
                let tokenPush = {
                    sToken: jwt.sign({ _id: savedUser._id.toHexString }, process.env.JWTPASSPHERE),
                    sPushToken: req.body.sPushToken,
                    sIpAddres: req.connection.remoteAddress
                }
                savedUser.aJwtTokens.push(tokenPush)
                savedUser.save().then(data => {
                    customerModel.filterUserData(data)
                    let AuthorizationData = {
                        Authorization: tokenPush.sToken
                    }
                    return endpoints.send(res, messages.custom('LOGIN_SUCC'), AuthorizationData, { 'Authorization': tokenPush.sToken })
                }).catch(error => {
                    debug("error :: ", error);
                    return endpoints.send(res, messages.server_error())
                })
            }).catch(error => {
                debug("error :: ", error);
                return endpoints.send(res, messages.server_error())
            })
        }
        // END GOOGLE LOGIN
    } catch (error) {
        debug('error :: ', error);
        return endpoints.send(res, messages.server_error())
    }
}

controllers.getProfile = (req, res) => {
    try {
        customerModel.findById({ _id: req._id }).then((userFound) => {
            if (!userFound) return endpoints.send(res, messages.not_found('user'));
            if (userFound.eIsActive === 'n') return endpoints.send(res, messages.inactive('user'));
            if (userFound.eIsActive === 'b') return endpoints.send(res, messages.inactive('user'));
            customerModel.filterUserData(userFound)
            return endpoints.send(res, messages.get_succ('User'), userFound);
        }).catch((error) => {
            debug('error :: ', error)
            return endpoints.send(res, messages.custom('FETCH_ERR'));
        })
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

controllers.updateProfile = (req, res) => {
    try {
        let body = _.pick(req.body, ['sFirstName', 'sLastName', 'sPhoneNumber', 'sCountry', 'sCurrency', 'sAddress'])
        if (body.sPhoneNumber) {
            customerModel.findOne({ sPhoneNumber: body.sPhoneNumber, _id: { $ne: req._id } }).then((customerFound) => {
                if (customerFound) return endpoints.send(res, messages.already_exists('Phone Number'))
                updateUserProfile(body)
            }).catch((error) => {
                debug('error :: ', error);
                return endpoints.send(res, messages.custom('FETCH_ERR'))
            })
        } else {
            updateUserProfile(body)
        }
        function updateUserProfile(body) {
            customerModel.findByIdAndUpdate(req._id, { $set: body }, { new: true }).then((updated_user) => {
                if (!updated_user) return endpoints.send(res, messages.not_found('user'))
                customerModel.filterUserData(updated_user)
                return endpoints.send(res, messages.updated('User'), updated_user)
            }).catch((error) => {
                debug('error :: ', error)
                return endpoints.send(res, messages.custom('FETCH_ERR'))
            })
        }
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

controllers.userForgotPasswordMail = (req, res) => {
    try {
        customerModel.findOne({ sEmail: req.body.sEmail, bUserStatus: true }).then((user) => {
            if (!user) return endpoints.send(res, messages.not_found('user'))
            if (user.eIsActive === 'b') return endpoints.send(res, messages.custom('USER_BLOCKED'))
            user.sVerificationToken = customerModel.createVerificationToken(user._id)  // jwt.sign({ _id: (user._id).toHexString() }, process.env.JWT_SECRET, { expiresIn: process.env.JWT_MAIL_VALIDITY });
            user.save().then((data) => {
                sendmail('forgot_password_mail.html',
                    {
                        SITE_NAME: process.env.SITE_NAME,
                        SITE_LOGO: `${process.env.SITE_LOGO}`,
                        USERNAME: user.sFirstName,
                        YEAR: new Date().getFullYear(),
                        ACTIVELINK: `${process.env.MAIL_HOST_LINK}/reset/${data.sVerificationToken}`
                    }, {
                        from: process.env.SMTP_FROM,
                        to: data.sEmail,
                        subject: "Forgot Password",
                    }).then(() => {
                        return endpoints.send(res, messages.custom('FORGOT_MAIL_SENT'))
                    }).catch(error => {
                        debug('error :: ', error)
                        return endpoints.send(res, messages.server_error())
                    })
            }).catch(error => {
                debug('error :: ', error);
                return endpoints.send(res, messages.custom('DATA_SAVE_ERR'))
            });

        }).catch((error) => {
            debug('error :: ', error);
            return endpoints.send(res, messages.custom('FETCH_ERR'))
        })
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }


}

controllers.userForgotPassword = (req, res) => {
    try {
        jwt.verify(req.body.sVerificationToken, process.env.JWT_SECRET, (err, decoded) => {
            if (err) return endpoints.send(res, messages.incorrect('token'))
            usersModel.findOne({ _id: decoded._id, sVerificationToken: req.body.sVerificationToken }).then((data) => {
                if (!data) return endpoints.send(res, messages.incorrect('token'))

                if (req.body.sNewPassword !== req.body.sNewRetypedPassword) {
                    return endpoints.send(res, messages.not_matched('Password'))
                }
                data.sPassword = req.body.sNewPassword;
                data.sVerificationToken = null;
                if (data.eIsActive !== 'b') {
                    data.eIsActive = 'y'
                }
                data.save().then(() => {
                    res.render('reset_password', {
                        SITE_NAME: process.env.SITE_NAME
                    });
                    // return endpoints.send(res, messages.custom('PASSWORD_CHANGED_SUCC'));
                }).catch(error => {
                    debug('error :: ', error)
                    return endpoints.send(res, messages.custom('DATA_SAVE_ERR'))
                });
            }).catch(error => {
                debug('error :: ', error)
                return endpoints.send(res, messages.custom('FETCH_ERR'))
            });
        });
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

controllers.userChangePassword = (req, res) => {
    try {
        if (!bcrypt.compareSync(req.body.sOldPassword, req.user.sPassword)) {
            return endpoints.send(res, messages.incorrect('Old Password is'))
        } else {
            req.user.sPassword = req.body.sNewPassword;
            req.user.save().then(data => {
                if (!data) return endpoints.send(res, messages.not_found('user'))
                return endpoints.send(res, messages.custom('PASSWORD_CHANGED_SUCC'))
            }).catch(error => {
                debug('error :: ', error)
                return endpoints.send(res, messages.custom('DATA_SAVE_ERR'))
            })
        }
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

controllers.userProfilePictureUpload = (req, res) => {
    try {
        uploadProfile(req, res, function (err) {
            if (err) return endpoints.send(res, messages.server_error());
            usersModel.findById(req._id, { sProfilePicture: 1 }).then((userFound) => {
                if (!userFound) return endpoints.send(res, messages.not_found('user'))
                var newProfilePicture = ''
                if (req.files.sProfilePicture) {
                    var myFileObject = req.files;
                    var sProfilePictureObj = myFileObject.sProfilePicture;
                    //Get Image Name based on uploaded path
                    if (mediaUploadPath == 's3') {
                        imgname = sProfilePictureObj[0].key;
                    } else {
                        imgname = sProfilePictureObj[0].filename;
                    }
                    newProfilePicture = imgname;
                    if (userFound.sProfilePicture && userFound.sProfilePicture != undefined) {
                        if (mediaUploadPath == 's3') {
                            //Delete object from s3 bucket 
                            var params = {
                                Bucket: process.env.awsS3Bucket,
                                Delete: {
                                    Objects: [
                                        {
                                            Key: userFound.sProfilePicture
                                        }
                                    ],
                                },
                            };
                            s3.deleteObjects(params, function (err, data) {
                                if (err) {
                                    console.log("error in removing image from s3", err)
                                };
                            });
                        } else {
                            //remove from local
                            var imgpath = process.env.profilePicturePath + '/' + userFound.sProfilePicture;
                            try {
                                const stats = fs.statSync(imgpath);
                                if (stats) {
                                    fs.unlinkSync(imgpath);
                                }
                            }
                            catch (err) {
                                console.log('file does not exist in local');
                            }
                        }
                    }
                    userFound.sProfilePicture = newProfilePicture
                    userFound.save().then((user) => {
                        if (!user) return endpoints.send(res, messages.not_found('user'));
                        return endpoints.send(res, messages.success(), user.sProfilePicture)
                    }).catch((error) => {
                        debug('error :: ', error)
                        return endpoints.send(res, messages.custom('DATA_SAVE_ERR'))
                    })
                } else {
                    return endpoints.send(res, messages.required_field('Image'))
                }
            }).catch((error) => {
                debug('error :: ', error)
                return endpoints.send(res, messages.custom('FETCH_ERR'))
            })
        });
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

controllers.verifyuserEmail = (req, res) => {
    try {
        let decoded = jwt.verify(req.params.sVerificationToken, process.env.JWT_SECRET);
        customerModel
            .findOne({ _id: decoded._id, sVerificationToken: req.params.sVerificationToken })
            .then((data) => {
                if (!data) {
                    return res.render('token_expire', { YEAR: new Date().getFullYear(), SITE_NAME: process.env.SITE_NAME });
                }
                data.eIsActive = 'y';
                data.sVerificationToken = null;
                data
                    .save()
                    .then((user) => {
                        res.render('account_activated', { user: user.sFirstName, SITE_NAME: process.env.SITE_NAME, YEAR: new Date().getFullYear() });
                    })
                    .catch((error) => {
                        debug('error :: ', error)
                        return endpoints.send(res, messages.custom('DATA_SAVE_ERR'))
                    });
            })
            .catch((error) => {
                debug('error :: ', error)
                return endpoints.send(res, messages.custom('FETCH_ERR'))
            });
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

controllers.reset = (req, res) => {
    try {
        usersModel
            .findOne({ sVerificationToken: req.params.sVerificationToken })
            .then((valid_user) => {
                if (!valid_user) return res.render('token_expire', { YEAR: new Date().getFullYear() });
                res.render('resetpassword', {
                    title: 'Reset password',
                    actionUrl: `${process.env.MAIL_HOST_LINK}/user/reset/password`,
                    sVerificationToken: req.params.sVerificationToken,
                    SITE_NAME: process.env.SITE_NAME
                });
            })
            .catch((error) => {
                debug('error :: ', error)
                return endpoints.send(res, messages.custom('FETCH_ERR'))
            });
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

controllers.userLogout = (req, res) => {
    try {
        customerModel.findOneAndUpdate({ _id: req._id }, { $pull: { aJwtTokens: { sToken: req.header('Authorization') } } }).then(() => {
            return endpoints.send(res, messages.logout());
        }).catch((error) => {
            debug('error :: ', error)
            return endpoints.send(res, messages.custom('FETCH_ERR'))
        });
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

controllers.contactUsCreate = (req, res) => {
    try {
        const body = _.pick(req.body, ['sFullName', 'sEmail', 'sSubject', 'sMessage'])
        let newInquery = new contactusModel(body)
        newInquery.save().then((inquerySaved) => {
            return endpoints.send(res, messages.success())
        }).catch((error) => {
            debug("error :: ", error);
            return endpoints.send(res, messages.server_error())
        })
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}




module.exports = controllers
