const { endpoints, messages } = require('@utils')
const { _ } = require('@utils/lib/helpers');
const validators = {}
const gift_model_name = "Gift card"



const validateEmail = function (email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

validators.updateProfile = async (req, res, next) => {
    const body = _.pick(req.body, ['sFirstName', 'sLastName'])
    if (!body.sFirstName) return endpoints.send(res, messages.required_field('firstName'), {})
    if (!body.sLastName) return endpoints.send(res, messages.required_field('lastName'), {})
    return next()
}

validators.addCategory = async (req, res, next) => {
    const body = _.pick(req.body, ['sTitle'])
    if (!body.sTitle) return endpoints.send(res, messages.required_field('Category title'), {})
    return next()
}

validators.addStatus = async (req, res, next) => {
    const body = _.pick(req.body, ['sTitle'])
    if (!body.sTitle) return endpoints.send(res, messages.required_field('Status title'), {})
    return next()
}

validators.addDisease = async (req, res, next) => {
    const body = _.pick(req.body, ['sTitle'])
    if (!body.sTitle) return endpoints.send(res, messages.required_field('Disease title'), {})
    return next()
}

validators.addTreatment = async (req, res, next) => {
    const body = _.pick(req.body, ['sTitle'])
    if (!body.sTitle) return endpoints.send(res, messages.required_field('Treatement title'), {})
    return next()
}

validators.list = async (req, res, next) => {
    const body = _.pick(req.body, ['size', 'search'])
    if (body.search && body.search != '' && typeof (body.search) != 'string') return endpoints.send(res, messages.custom('SEARCH_STRING'))
    if (!body.size) return endpoints.send(res, messages.required_field('size'), {})
    if (typeof (body.size) != 'number') return endpoints.send(res, messages.incorrect('size'), {})
    return next()
}

validators.addSubAdmin = async (req, res, next) => {
    const body = _.pick(req.body, ['sFirstName', 'sLastName', 'sEmail', 'sPassword'])
    if (!body.sFirstName) return endpoints.send(res, messages.required_field('FirstName'), {})
    if (!body.sLastName) return endpoints.send(res, messages.required_field('LastName'), {})
    if (!body.sEmail) return endpoints.send(res, messages.required_field('Email'), {})
    if (!body.sPassword) return endpoints.send(res, messages.required_field('Password'), {})
    return next()
}

validators.addContent = async (req, res, next) => {
    const body = _.pick(req.body, ['sDesc'])
    if (!body.sDesc) return endpoints.send(res, messages.required_field('Content Description'), {})
    return next()
}

validators.addCms = async (req, res, next) => {
    const body = _.pick(req.body, ['sPageName', 'sDesc'])
    if (!body.sPageName) return endpoints.send(res, messages.required_field('PageName'), {})
    if (!body.sDesc) return endpoints.send(res, messages.required_field('Description'), {})
    return next()
}

validators.addHospital = async (req, res, next) => {
    const body = _.pick(req.body, ['sName', 'iCityId'])
    if (!body.sName) return endpoints.send(res, messages.required_field('Hospital Name'), {})
    if (!body.iCityId) return endpoints.send(res, messages.required_field('City'), {})
    return next()
}

validators.addGiftCard = async (req, res, next) => {

    const body = _.pick(req.body, ['sTitle', 'sValidity', 'nQuantity', 'nPrice'])
    if (!body.sTitle) return endpoints.send(res, messages.required_field(`${gift_model_name} title`), {})
    if (!body.sValidity) return endpoints.send(res, messages.required_field(`${gift_model_name} validity`), {})
    if (!body.nQuantity) return endpoints.send(res, messages.required_field(`${gift_model_name} quantity`), {})
    if (!body.nPrice) return endpoints.send(res, messages.required_field(`${gift_model_name} price`), {})
    return next()
}

validators.addInvestigator = async (req, res, next) => {
    const body = _.pick(req.body, ['iHospitalId', 'sName', 'sEmail'])
    if (!body.iHospitalId) return endpoints.send(res, messages.required_field('Hospital'), {})
    if (!body.sName) return endpoints.send(res, messages.required_field('Investigator Name'), {})
    if (!body.sEmail) return endpoints.send(res, messages.required_field('Email'), {})
    return next()
}

validators.addCountry = async (req, res, next) => {
    const body = _.pick(req.body, ['sName'])
    if (!body.sName) return endpoints.send(res, messages.required_field('Country Name'), {})
    return next()
}
validators.faq = async (req, res, next) => {
    const body = _.pick(req.body, ['sQuestion', 'sAnswer'])
    if (!body.sQuestion) return endpoints.send(res, messages.required_field('sQuestion'), {})
    if (!body.sAnswer) return endpoints.send(res, messages.required_field('sAnswer'), {})

    return next()
}


module.exports = validators