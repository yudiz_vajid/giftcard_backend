// const services = require('./services')
const { messages, endpoints, log } = require('../../../utils')
const { _ } = require('@utils/lib/helpers');
const { sharpImage } = require('@utils/lib/services/imageresize');
const controllers = {}
const usersModel = require('@models/users.model')
var ObjectId = require('mongodb').ObjectID;
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const { sendmail } = require('@utils/lib/services/nodemailer');
const { removenull } = require('@utils/lib/helpers');
const mime = require('mime');
const AWS = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');
const debug = require('debug')('http')

var mediaUploadPath = process.env.siteMediaUploadPath;
AWS.config.update({ accessKeyId: process.env.awsAccesskeyID, secretAccessKey: process.env.awsSecretAccessKey });

var s3 = new AWS.S3();
if (mediaUploadPath == 's3') {
    // User Profile image upload on live domain - s3 Bucket
    var storage = multerS3({
        s3: s3,
        bucket: process.env.awsS3Bucket + '/profilePictures',
        acl: 'public-read',
        key: function (req, file, callback) {
            var fileName = `${req._id}_${Date.now()}`;
            callback(null, fileName + '.' + mime.getExtension(file.mimetype));
        }
    });
} else {
    // User Profile image upload on local domain
    var storage = multer.diskStorage({
        destination: function (req, file, callback) {
            callback(null, process.env.profilePicturePath);
        },
        filename: function (req, file, callback) {
            var fileName = `${req._id}_${Date.now()}`;
            callback(null, fileName + '.' + mime.getExtension(file.mimetype));
        }
    });
}

var uploadProfile = multer({
    storage: storage
}).fields([{
    name: 'sProfilePicture',
    maxCount: 1
}]);


const generateAuthToken = function (user) {
    let obj = {
        sToken: jwt.sign({
            _id: (user._id).toHexString()
        }, process.env.JWTPASSPHERE)
    };
    if (user.aJwtTokens.length < process.env.ADMIN_LOGIN_LIMIT || process.env.ADMIN_LOGIN_LIMIT == 0) {
        user.aJwtTokens.push(obj);
    } else {
        user.aJwtTokens.splice(0, 1);
        user.aJwtTokens.push(obj);
    }
    // user.aJwtTokens.push(obj);
    return user.save().then(function () {
        return obj.sToken;
    }).catch((error) => {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error());
    });
}

controllers.adminLogin = (req, res) => {
    try {
        let body = _.pick(req.body, ['sEmail', 'sPassword'])
        usersModel.findOne({ sEmail: body.sEmail, bUserStatus: true, eUserType: 'admin' }).then((user) => {
            if (!user) return endpoints.send(res, messages.custom('LOGIN_FAIL'))
            if (bcrypt.compareSync(body.sPassword, user.sPassword)) {
                if (user.eIsActive === 'n') return endpoints.send(res, messages.custom('VERIFY_EMAIL'))
                if (user.eIsActive === 'b') return endpoints.send(res, messages.custom('USER_BLOCKED'))
                usersModel.findById({ _id: ObjectId(user._id) }).then((updated_user) => {
                    generateAuthToken(updated_user).then((token) => {
                        if (!token) return endpoints.send(res, messages.server_error())
                        let AuthorizationData = {
                            userId: updated_user._id,
                            Authorization: token,
                            aPermissions: updated_user.aPermissions,
                            sFirstName: updated_user.sFirstName,
                            sLastName: updated_user.sLastName,
                            sProfilePicture: updated_user.sProfilePicture,
                            sEmail: updated_user.sEmail
                        }
                        endpoints.send(res, messages.custom('LOGIN_SUCC'), AuthorizationData, { 'Authorization': token })
                    }).catch((error) => {
                        debug('error :: ', error)
                        return endpoints.send(res, messages.server_error())
                    })
                });
            } else {
                return endpoints.send(res, messages.custom('LOGIN_FAIL'))
            }
        }).catch((error) => {
            debug('error :: ', error)
            return endpoints.send(res, messages.custom('FETCH_ERR'))
        })
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

controllers.getProfile = (req, res) => {
    try {
        console.log("req :: ", req._id);

        usersModel.findById({ _id: req._id }).then((userFound) => {
            if (!userFound) return endpoints.send(res, messages.not_found('user'));
            if (userFound.eIsActive === 'n') return endpoints.send(res, messages.custom('VERIFY_EMAIL'))
            if (userFound.eIsActive === 'b') return endpoints.send(res, messages.custom('USER_BLOCKED'))
            usersModel.filterUserData(userFound)
            return endpoints.send(res, messages.get_succ('Admin'), userFound);
        }).catch((error) => {
            debug('error :: ', error)
            return endpoints.send(res, messages.custom('FETCH_ERR'));
        })
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

controllers.updateProfile = (req, res) => {
    try {
        let body = _.pick(req.body, ['sFirstName', 'sLastName', 'sPhoneNumber'])
        // removenull(body)
        if (body.sPhoneNumber) {
            usersModel.findOne({ sPhoneNumber: body.sPhoneNumber, _id: { $ne: req._id } }).then((number_exist) => {
                if (number_exist) return endpoints.send(res, messages.already_exists('Phone Number'))
                updateUser(body)
            }).catch((error) => {
                debug('error :: ', error)
                return endpoints.send(res, messages.custom('FETCH_ERR'))
            })
        } else {
            updateUser(body)
        }
        function updateUser(body) {
            usersModel.findByIdAndUpdate(req._id, { $set: body }, { new: true }).then((updated_user) => {
                if (!updated_user) return endpoints.send(res, messages.not_found('user'))
                usersModel.filterUserData(updated_user)
                return endpoints.send(res, messages.updated_succ("Admin"), updated_user)
            }).catch((error) => {
                debug('error :: ', error)
                return endpoints.send(res, messages.custom('FETCH_ERR'))
            })
        }
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

controllers.adminForgotPasswordMail = (req, res) => {
    try {
        usersModel.findOne({ sEmail: req.body.sEmail, bUserStatus: true, eUserType: 'admin' }).then((user) => {
            if (!user) return endpoints.send(res, messages.custom('NOT_REG_EMAIL'))
            if (user.eIsActive === 'b') return endpoints.send(res, messages.custom('USER_BLOCKED'))
            user.sVerificationToken = usersModel.createVerificationToken(user._id)  // jwt.sign({ _id: (user._id).toHexString() }, process.env.JWT_SECRET, { expiresIn: process.env.JWT_MAIL_VALIDITY });
            user.save().then((data) => {
                sendmail('forgot_password_mail.html',
                    {
                        SITE_NAME: process.env.SITE_NAME,
                        SITE_LOGO: process.env.SITE_LOGO,
                        YEAR: new Date().getFullYear(),
                        USERNAME: user.sFirstName,
                        ACTIVELINK: `${process.env.FRONT_URL}/reset/${data.sVerificationToken}`
                    }, {
                        from: process.env.SMTP_FROM,
                        to: data.sEmail,
                        subject: "Forgot Password",
                    }).then(() => {
                        return endpoints.send(res, messages.custom('FORGOT_MAIL_SENT'), { sEmail: req.body.sEmail })
                    }).catch(error => {
                        debug('error :: ', error)
                        return endpoints.send(res, messages.server_error())
                    })
            }).catch(error => {
                debug('error :: ', error)
                return endpoints.send(res, messages.custom('DATA_SAVE_ERR'))
            });

        }).catch((error) => {
            debug('error :: ', error)
            return endpoints.send(res, messages.custom('FETCH_ERR'))
        })
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

controllers.adminForgotPassword = (req, res) => {
    try {
        jwt.verify(req.body.sVerificationToken, process.env.JWT_SECRET, (err, decoded) => {
            if (err) return endpoints.send(res, messages.incorrect('token'))
            usersModel.findOne({ _id: decoded._id, sVerificationToken: req.body.sVerificationToken, eUserType: 'admin' }).then((data) => {
                if (!data) return endpoints.send(res, messages.incorrect('token'))
                if (req.body.sNewPassword !== req.body.sNewRetypedPassword) {
                    return endpoints.send(res, messages.not_matched('Password'))
                }
                data.sPassword = req.body.sNewPassword;
                data.sVerificationToken = null;
                if (data.eIsActive !== 'b') {
                    data.eIsActive = 'y'
                }
                data.save().then(() => {
                    return endpoints.send(res, messages.custom('PASSWORD_CHANGED_SUCC'));
                }).catch(error => {
                    debug('error :: ', error)
                    return endpoints.send(res, messages.custom('DATA_SAVE_ERR'))
                });
            }).catch(error => {
                debug('error :: ', error)
                return endpoints.send(res, messages.custom('FETCH_ERR'))
            });
        });
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

controllers.adminProfilePictureUpload = (req, res) => {
    try {
        uploadProfile(req, res, function (err) {
            if (err) return endpoints.send(res, messages.server_error());
            usersModel.findById(req._id, { sProfilePicture: 1 }).then((userFound) => {
                if (!userFound) return endpoints.send(res, messages.not_found('user'))
                var newProfilePicture = ''
                if (req.files.sProfilePicture) {
                    var myFileObject = req.files;
                    var sProfilePictureObj = myFileObject.sProfilePicture;
                    //Get Image Name based on uploaded path
                    if (mediaUploadPath == 's3') {
                        imgname = sProfilePictureObj[0].key;
                    } else {
                        imgname = sProfilePictureObj[0].filename;
                    }
                    newProfilePicture = imgname;
                    if (userFound.sProfilePicture && userFound.sProfilePicture != undefined) {
                        if (mediaUploadPath == 's3') {
                            //Delete object from s3 bucket 
                            var params = {
                                Bucket: process.env.awsS3Bucket,
                                Delete: {
                                    Objects: [
                                        {
                                            Key: userFound.sProfilePicture
                                        }
                                    ],
                                },
                            };
                            s3.deleteObjects(params, function (err, data) {
                                if (err) {
                                    console.log("error in removing image from s3", err)
                                };
                            });
                        } else {
                            //remove from local
                            var imgpath = process.env.profilePicturePath + '/' + userFound.sProfilePicture;
                            try {
                                const stats = fs.statSync(imgpath);
                                if (stats) {
                                    fs.unlinkSync(imgpath);
                                }
                            }
                            catch (err) {
                                console.log('file does not exist in local');
                            }
                        }
                    }
                    userFound.sProfilePicture = newProfilePicture
                    userFound.save().then((user) => {
                        if (!user) return endpoints.send(res, messages.not_found('user'));
                        return endpoints.send(res, messages.updated_succ('Profile Picture'), user.sProfilePicture)
                    }).catch((error) => {
                        debug('error :: ', error)
                        return endpoints.send(res, messages.custom('DATA_SAVE_ERR'))
                    })
                } else {
                    return endpoints.send(res, messages.required_field('Image'))
                }
            }).catch((error) => {
                debug('error :: ', error)
                return endpoints.send(res, messages.custom('FETCH_ERR'))
            })
        });
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

controllers.getCommonUserData = (req, res) => {
    try {
        let verificationToken = req.body.token
        usersModel.findByToken(verificationToken).then((user) => {
            if (user && user.eUserType === 'admin') {
                let userData = {
                    sFirstName: user.sFirstName,
                    sLastName: user.sLastName,
                    sProfilePicture: user.sProfilePicture
                }
                return endpoints.send(res, messages.success(), userData)
            }
        }).catch(error => {
            debug('error :: ', error)
            return endpoints.send(res, messages.server_error())
        });
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

controllers.verifyToken = (req, res) => {
    try {
        jwt.verify(req.params.sVerificationToken, process.env.JWT_SECRET, (err, decoded) => {
            if (err) return endpoints.send(res, messages.custom('LINK_EXP'))
            usersModel.findOne({ _id: decoded._id, sVerificationToken: req.params.sVerificationToken, eUserType: 'admin' }).then((data) => {
                if (!data) return endpoints.send(res, messages.custom('LINK_EXP'))
                return endpoints.send(res, messages.success())
            }).catch(error => {
                debug('error :: ', error)
                return endpoints.send(res, messages.custom('FETCH_ERR'))
            });
        });
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

controllers.adminChangePassword = (req, res) => {
    try {
        if (!bcrypt.compareSync(req.body.sOldPassword, req.user.sPassword)) {
            return endpoints.send(res, messages.incorrect('Old Password is'))
        } else {
            req.user.sPassword = req.body.sNewPassword;
            req.user.save().then(data => {
                if (!data) return endpoints.send(res, messages.not_found('user'))
                return endpoints.send(res, messages.custom('PASSWORD_CHANGED_SUCC'))
            }).catch(error => {
                debug('error :: ', error)
                return endpoints.send(res, messages.custom('DATA_SAVE_ERR'))
            })
        }
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

controllers.sharpimage = (req, res) => {
    try {
        const stats = fs.statSync(req.query.src);
        if (stats) {
            let params = req.query
            res.header('Content-Type', "image/*")
            sharpImage(params.src, parseInt(params.w), parseInt(params.h), parseInt(params.q)).pipe(res);
        }
    }
    catch (err) {
        return endpoints.send(res, messages.not_found('Image'))
    }
}

controllers.verifiedAdmin = (req, res) => {
    return endpoints.send(res, messages.success(), req.user)
}


module.exports = controllers