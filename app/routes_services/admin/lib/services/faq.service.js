const questionModel = require('@models/question.model')
const commonService = require('./common.service');
const question_model_name = 'Question'
const { _ } = require('@utils/lib/helpers');


const createQuestion = (req, res, next) => {
    const body = _.pick(req.body, ['sQuestion', 'sAnswer'])
    // let newFaq = new questionModel(body)
    commonService.addItem(questionModel, 'sQuestion', question_model_name, body, req, res);
}

const list = (req, res, next) => {
    commonService.listItem(cmsModel, Cms_model_name, req, res);
}


module.exports = {
    createQuestion,
    list
}