const { _ } = require('@utils/lib/helpers');
const { endpoints, messages } = require('@utils')
const { removenull } = require('@utils/lib/helpers');
const commonService = require('./common.service');
const giftCard_model_name = 'Gift Card'
const category_model_name = 'Category'
const AWS = require('aws-sdk')
const multer = require('multer')
const mime = require('mime');
const multerS3 = require('multer-s3')
const ObjectId = require('mongodb').ObjectID;
const debug = require('debug')('http')
var mediaUploadPath = process.env.siteMediaUploadPath;
AWS.config.update({ accessKeyId: process.env.awsAccesskeyID, secretAccessKey: process.env.awsSecretAccessKey });
const cardModel = require('@models/giftcard.model.js')
const categoriesModel = require('@models/category.model')
var s3 = new AWS.S3();

if (mediaUploadPath == 's3') {
    var storage = multerS3({
        s3: s3,
        bucket: process.env.S3_BUCKET_NAME + '/' + 'cardImages',
        acl: 'public-read',
        key: function (req, file, callback) {
            callback(null, `${req._id}_${Date.now()}_${file.originalname}`)
        }
    })
} else {
    var storage = multer.diskStorage({
        destination: function (req, file, callback) {
            callback(null, process.env.CARD_IMAGE_PATH);
        },
        filename: function (req, file, callback) {
            var fileName = `${req._id}_${Date.now()}`;
            callback(null, fileName + '.' + mime.getExtension(file.mimetype));
        }
    });

}

let upload = multer({
    storage: storage
}).single('sCardImage')



const addCard = (req, res) => {
    upload(req, res, function (err) {
        if (err) return endpoints.send(res, messages.custom('Image required'));
        let body = _.pick(req.body, ['sTitle', 'iCategoryId', 'sDate', 'sTime', 'sValidity', 'nQuantity', 'nOffer', 'sOfferValidity', 'nPrice', 'sDescription', 'sCardImage'])
        if (!body.sTitle) return endpoints.send(res, messages.required_field(`${giftCard_model_name} title`), {})
        if (!body.iCategoryId) return endpoints.send(res, messages.required_field(`${giftCard_model_name} Category Id`), {})
        if (!body.sValidity) return endpoints.send(res, messages.required_field(`${giftCard_model_name} validity`), {})
        if (!body.nQuantity) return endpoints.send(res, messages.required_field(`${giftCard_model_name} quantity`), {})
        if (!body.nPrice) return endpoints.send(res, messages.required_field(`${giftCard_model_name} price`), {})
        if (req.file != undefined) {
            body.sCardImage = req.file.filename
        }
        removenull(body)
        if (body.nOffer) {
            if (!body.sOfferValidity) return endpoints.send(res, messages.required_field(`${giftCard_model_name} Offer Validity`), {})
        }
        categoriesModel.findById(body.iCategoryId).then((categpryFound) => {
            if (!categpryFound) return endpoints.send(res, messages.not_found(category_model_name))
            cardModel.findOne({ sTitle: body.sTitle, sValidity: body.sValidity }).then((cardFound) => {
                if (cardFound) {
                    return endpoints.send(res, messages.already_exists(giftCard_model_name))
                }
                let newCard = new cardModel(body)
                newCard.save(body).then((savedCard) => {
                    return endpoints.send(res, messages.created(giftCard_model_name), savedCard)
                }).catch((error) => {
                    return endpoints.send(res, messages.custom('CARD_SAVE_ERR'))
                })
            }).catch((error) => {
                return endpoints.send(res, messages.server_error());
            })
        }).catch((error) => {
            console.log("error :: ", error);
            return endpoints.send(res, messages.server_error());
        })
    })
}

const viewCard = (req, res) => {
    commonService.viewItem(cardModel, giftCard_model_name, req, res);
}

const editCard = (req, res) => {
    upload(req, res, function (err) {
        let itemId = req.params.iItemId
        let body = _.pick(req.body, ['sTitle', 'sDate', 'sTime', 'sValidity', 'nOffer', 'sOfferValidity', 'nPrice', 'sDescription', 'sCardImage'])
        removenull(body)
        cardModel.findById(itemId).then((cardFound) => {
            if (!cardFound) return endpoints.send(res, messages.not_found(giftCard_model_name))
            cardModel.findOne({ sTitle: body.sTitle, sValidity: body.sValidity, _id: { $ne: itemId } }).then((updatedCard) => {
                if (updatedCard) return endpoints.send(res, messages.already_exists(giftCard_model_name))
                cardFound.sTitle = body.sTitle
                cardFound.sDate = body.sDate
                cardFound.sTime = body.sTime
                cardFound.sValidity = body.sValidity
                cardFound.nOffer = body.nOffer
                cardFound.sOfferValidity = body.sOfferValidity
                cardFound.nPrice = body.nPrice
                cardFound.sDescription = body.sDescription
                cardFound.sCardImage = body.sCardImage
                cardFound.dUpdatedAt = new Date();

                cardFound.save().then((savedCard) => {
                    return endpoints.send(res, messages.updated_succ(giftCard_model_name), savedCard)
                }).catch((error) => {
                    console.log("error :: ", error);
                    return endpoints.send(res, messages.server_error())
                })

            }).catch((error) => {
                console.log("error :: ", error);
                return endpoints.send(res, messages.server_error())
            })
        }).catch((error) => {
            console.log("error :: ", error);
            return endpoints.send(res, messages.server_error())
        })
    })
}

const deleteCard = (req, res) => {
    cardModel.findByIdAndUpdate(ObjectId(req.params.iItemId), { $set: { bCardStatus: false } }, { new: true }).then((item) => {
        if (!item) return endpoints.send(res, messages.not_found(giftCard_model_name))
        return endpoints.send(res, messages.delete_succ(giftCard_model_name), item)
    }).catch((error) => {
        debug('error :: ', error)
        return endpoints.send(res, messages.custom('REMOVE_ERR'))
    })
}

const changeStatus = (req, res) => {
    commonService.changeItemStatus(cardModel, giftCard_model_name, req, res);
}

const list = (req, res, next) => {
    try {
        if (!req.body.start) {
            req.body.start = 0
        }
        let search = ''
        if (req.body.search && req.body.search != '') {
            search = req.body.search
                .replace(/\\/g, '\\\\')
                .replace(/\$/g, '\\$')
                .replace(/\*/g, '\\*')
                .replace(/\)/g, '\\)')
                .replace(/\(/g, '\\(')
                .replace(/'/g, "\\'")
                .replace(/"/g, '\\"').trim();
        }

        let sort;
        if (req.body.sortby && req.body.sortby != '') {
            let stype = req.body.sortby;
            let sdir = (req.body.orderby === 'desc') ? -1 : 1;
            sort = {
                [stype]: sdir
            }
        } else {
            sort = {
                'dCreatedAt': -1
            }
        }

        var and;
        if (!req.body.bStatus || req.body.bStatus == 'All' || req.body.bStatus == '') {
            and = [{
                bCardStatus: true
            }]
        }
        let query = {
            $and: and,
            $or: [
                {
                    sTitle: { $regex: new RegExp('^.*' + search + '.*', 'i') }
                },
                {
                    sPageName: { $regex: new RegExp('^.*' + search + '.*', 'i') }
                },
                {
                    sName: { $regex: new RegExp('^.*' + search + '.*', 'i') }
                }
            ]
        }


        let aggrigationQuery = [{
            $facet: {
                "data": [{
                    $match: query
                },
                {
                    $sort: sort
                },
                {
                    $skip: parseInt(req.body.start)
                },
                {
                    $limit: parseInt(req.body.size)
                },
                {
                    $project: {
                        sTitle: 1,
                        sValidity: 1,
                        nQuantity: 1,
                        nOffer: 1,
                        sOfferValidity: 1,
                        nPrice: 1,
                        eIsActive: 1
                    }
                }
                ],
                "count": [{
                    $match: query
                }, {
                    $count: "totalData"
                }]
            }
        }]

        cardModel.aggregate(aggrigationQuery, (error, docs) => {
            if (error) return endpoints.send(res, messages.custom('FETCH_ERR'))
            if (docs[0].data.length <= 0) docs[0].count[0] = { "totalData": 0 }
            docs[0].data.map(singleItem => {
                if (singleItem.iCityId) {
                    var cityName = cities.filter(function (item) {
                        return item.id == singleItem.iCityId
                    });
                    singleItem.sCityName = cityName[0].name
                }
            })
            let result = {
                data: docs[0].data,
                count: docs[0].count[0]
            }
            return endpoints.send(res, messages.list_succ(giftCard_model_name), result)
        });
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}


const simpleSingleList = (req, res) => {
    cardModel.distinct("sTitle").then((giftcardFound) => {
        return endpoints.send(res, messages.list_succ(giftCard_model_name), giftcardFound)
    }).catch((error) => {
        console.log("error :: ", error);
        return endpoints.send(res, messages.server_error())
    });
}


const getCardListByName = (req, res) => {
    cardModel.find({ sTitle: req.body.sTitle }).then((cardFound) => {
        return endpoints.send(res, messages.list_succ(giftCard_model_name), cardFound)
    }).catch((error) => {
        console.log("error :: ", error);
        return endpoints.send(res.messages.server_error())
    });
}

module.exports = {
    addCard,
    viewCard,
    editCard,
    deleteCard,
    changeStatus,
    list,
    simpleSingleList,
    getCardListByName
}
