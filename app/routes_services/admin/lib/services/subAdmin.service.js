const { messages, endpoints } = require('@utils')
const { _ } = require('@utils/lib/helpers');
const jwt = require('jsonwebtoken');
const { sendmail } = require('@utils/lib/services/nodemailer');
const { removenull } = require('@utils/lib/helpers');
const usersModel = require('@models/users.model');
const commonService = require('./common.service');
const ObjectId = require('mongodb').ObjectID;
const subAdmin_model_name = 'SubAdmin'
const debug = require('debug')('http')

const createSubAdmin = (req, res) => {
    try {
        let body = _.pick(req.body, ['sFirstName', 'sLastName', 'sEmail', 'sPassword', 'sPhoneNumber', 'aPermissions']);
        console.log("body :: ", body);

        removenull(body)
        if (body.sFirstName) {
            var splitsFirstName = body.sFirstName.toLowerCase().split(' ')
            for (let i = 0; i < splitsFirstName.length; i++) {
                splitsFirstName[i] = splitsFirstName[i].charAt(0).toUpperCase() + splitsFirstName[i].substring(1)
            }
            body.sFirstName = splitsFirstName.join(' ')
        }
        if (body.sLastName) {
            var splitsLastName = body.sLastName.toLowerCase().split(' ')
            for (let i = 0; i < splitsLastName.length; i++) {
                splitsLastName[i] = splitsLastName[i].charAt(0).toUpperCase() + splitsLastName[i].substring(1)
            }
            body.sLastName = splitsLastName.join(' ')
        }

        body.eUserType = 'admin'
        if (body.sPhoneNumber) {
            var query = {
                $or: [{ sEmail: body.sEmail }, { sPhoneNumber: body.sPhoneNumber }]
            }
        } else {
            var query = {
                sEmail: body.sEmail
            }
        }
        usersModel.findOne(query).then((user_found) => {
            if (user_found) {
                if (body.sPhoneNumber && body.sPhoneNumber === user_found.sPhoneNumber) return endpoints.send(res, messages.already_exists('PhoneNumber'))
                if (body.sEmail && body.sEmail === user_found.sEmail) return endpoints.send(res, messages.already_exists('Email'))
            }
            let subAdmin = new usersModel(body)
            subAdmin.save().then((subAdmin) => {
                subAdmin.sVerificationToken = usersModel.createVerificationToken(subAdmin._id)
                subAdmin.save().then((data) => {
                    sendmail('account_active_subadmin.html',
                        {
                            USERNAME: data.sFirstName,
                            SITE_NAME: process.env.SITE_NAME,
                            YEAR: new Date().getFullYear(),
                            SITE_LOGO: process.env.SITE_LOGO,
                            PASSWORD: body.sPassword,
                            ACTIVELINK: `${process.env.FRONT_URL}/Verificationsub/${data.sVerificationToken}`
                        }, {
                            from: process.env.SMTP_FROM,
                            to: data.sEmail,
                            subject: "Email Verification",
                        }).then(() => {
                            return endpoints.send(res, messages.created(subAdmin_model_name), subAdmin)
                        }).catch(error => {
                            console.log("error : ", error);

                            // Remove user from DB due to email is not sent
                            data.remove().then(() => {
                                return endpoints.send(res, messages.custom('MAIL_SENT_FAIL'))
                            })
                        });
                }).catch(error => {
                    debug('error :: ', error)
                    return endpoints.send(res, messages.custom('DATA_SAVE_ERR'))
                });
            }).catch((error) => {
                debug('error :: ', error)
                return endpoints.send(res, messages.custom('SUB_ADMIN_SAVE_ERR'))
            })
        }).catch((error) => {
            debug('error :: ', error)
            return endpoints.send(res, messages.custom('FETCH_ERR'))
        })
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

const subAdminList = (req, res) => {
    try {
        if (!req.body.pageNumber) {
            req.body.pageNumber = 0
        }
        let search = ''
        if (req.body.search && req.body.search != '') {
            search = req.body.search
                .replace(/\\/g, '\\\\')
                .replace(/\$/g, '\\$')
                .replace(/\*/g, '\\*')
                .replace(/\)/g, '\\)')
                .replace(/\(/g, '\\(')
                .replace(/'/g, "\\'")
                .replace(/"/g, '\\"').trim();
        }
        let sort;
        if (req.body.sortby && req.body.sortby != '') {
            let stype = req.body.sortby;
            let sdir = (req.body.orderby === 'desc') ? -1 : 1;
            sort = {
                [stype]: sdir
            }
        } else {
            sort = {
                'dCreatedAt': -1
            }
        }
        var and = [{ bUserStatus: true }, { eUserType: 'admin' }, { _id: { $ne: ObjectId(req._id) } }]
        let query = {
            $and: and,
            $or: [{
                sFirstName: { $regex: new RegExp('^.*' + search + '.*', 'i') }
            },
            {
                sLastName: { $regex: new RegExp('^.*' + search + '.*', 'i') }
            },
            {
                sEmail: { $regex: new RegExp('^.*' + search + '.*', 'i') }
            },
            {
                sName: { $regex: new RegExp('^.*' + search + '.*', 'i') }
            },
                // {
                //     sPhoneNumber: {
                //         $regex: '^' + search,
                //         $options: 'i'
                //     }
                // }
            ]
        }
        let aggrigationQuery = [
            {
                $addFields: {
                    "sFullName": { $concat: ["$sFirstName", " ", "$sLastName"] }
                }
            },
            {
                $facet: {
                    "data": [
                        {
                            $match: query
                        },
                        {
                            $sort: sort
                        },
                        {
                            $skip: parseInt(req.body.start)
                        },
                        {
                            $limit: parseInt(req.body.size)
                        },
                        {
                            $project: {
                                sFullName: 1,
                                sEmail: 1,
                                sPhoneNumber: 1,
                                eIsActive: 1
                            }
                        }
                    ],
                    "count": [{
                        $match: query
                    }, {
                        $count: "totalData"
                    }]
                }
            }]

        usersModel.aggregate(aggrigationQuery, (error, docs) => {
            if (error) return endpoints.send(res, messages.custom('FETCH_ERR'))
            if (docs[0].data.length <= 0) docs[0].count[0] = { "totalData": 0 }
            let result = {
                data: docs[0].data,
                count: docs[0].count[0]
            }
            if (docs.length) return endpoints.send(res, messages.list_succ(subAdmin_model_name), result)
            return endpoints.send(res, messages.list_succ(itemName), result)
        });
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

const viewSubAdmin = (req, res) => {
    commonService.viewItem(usersModel, subAdmin_model_name, req, res)
}


const deleteSubAdmin = (req, res) => {
    commonService.softDeleteItem(usersModel, subAdmin_model_name, req, res)
}

const changeStatus = (req, res) => {
    let body = {
        eIsActive: req.body.eIsActive
    }
    usersModel.findByIdAndUpdate(req.body.iItemId, { $set: body }, { new: true }).then((updated_subAdmin) => {
        if (!updated_subAdmin) return endpoints.send(res, messages.not_found(subAdmin_model_name))
        return endpoints.send(res, messages.updated(subAdmin_model_name), updated_subAdmin)
    }).catch((error) => {
        debug('error :: ', error)
        return endpoints.send(res, messages.catch('FETCH_ERR'))
    })
}

const editSubAdmin = (req, res) => {
    try {
        let body = {
            sFirstName: req.body.sFirstName,
            sLastName: req.body.sLastName,
            sEmail: req.body.sEmail,
            sPhoneNumber: req.body.sPhoneNumber,
            aPermissions: req.body.aPermissions
        }
        // removenull(body);
        usersModel.findOne({ sEmail: body.sEmail, _id: { $ne: ObjectId(req.params.iItemId) } }).then((user_found) => {
            if (user_found && user_found.sEmail === body.sEmail) return endpoints.send(res, messages.already_exists('Email'))
            if (body.sPhoneNumber && body.sPhoneNumber != '') {
                usersModel.findOne({ sEmail: body.sPhoneNumber, _id: { $ne: ObjectId(req.params.iItemId) } }).then((number_found) => {
                    if (number_found && number_found.sPhoneNumber === body.sPhoneNumber) return endpoints.send(res, messages.already_exists('PhoneNumber'))
                    updateUser(body);
                }).catch((error) => {
                    debug('error :: ', error)
                    return endpoints.send(res, messages.custom('FETCH_ERR'))
                })
            } else {
                updateUser(body)
            }
            function updateUser(body) {
                usersModel.findByIdAndUpdate(req.params.iItemId, { $set: body }, { new: true }).then((subAdmin_updated) => {
                    if (!subAdmin_updated) return endpoints.send(res, messages.not_found(subAdmin_model_name))
                    subAdmin_updated.aJwtTokens = undefined;
                    return endpoints.send(res, messages.updated(subAdmin_model_name), subAdmin_updated)
                }).catch((error) => {
                    debug('error :: ', error)
                    return endpoints.send(res, messages.custom('UPDATE_ERR'))
                })
            }
        }).catch((error) => {
            debug('error :: ', error)
            return endpoints.send(res, messages.custom('FETCH_ERR'))
        })
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

const verifyMail = (req, res) => {
    let decoded = jwt.verify(req.params.sVerificationToken, process.env.JWT_SECRET);
    usersModel
        .findOne({ _id: decoded._id, sVerificationToken: req.params.sVerificationToken })
        .then((data) => {
            if (!data) {
                // return res.render('token_expire', { YEAR: new Date().getFullYear() });
                return endpoints.send(res, messages.custom('VERIFY_EMAIL_LINK_EXP'))
            }
            data.eIsActive = 'y';
            data.sVerificationToken = null;
            data
                .save()
                .then((user) => {
                    // res.render('account_activated', { user: user.sFirstName, YEAR: new Date().getFullYear() });
                    return endpoints.send(res, messages.success());
                })
                .catch((error) => {
                    debug('error :: ', error)
                    return endpoints.send(res, messages.custom('DATA_SAVE_ERR'))
                });
        })
        .catch((error) => {
            debug('error :: ', error)
            return endpoints.send(res, messages.custom('FETCH_ERR'))
        });
}

module.exports = {
    createSubAdmin,
    subAdminList,
    viewSubAdmin,
    deleteSubAdmin,
    changeStatus,
    editSubAdmin,
    verifyMail
}
