const { _ } = require('@utils/lib/helpers');
const { removenull } = require('@utils/lib/helpers');
const categoriesModel = require('@models/category.model')
const commonService = require('./common.service');
const category_model_name = 'Category'

const addCategory = (req, res) => {
    let body = _.pick(req.body, ['sName'])
    removenull(body)
    commonService.addItem(categoriesModel, 'sName', category_model_name, body, req, res);
}


const list = (req, res, next) => {
    commonService.listDynamicItem(categoriesModel, category_model_name, req, res);
}


const viewCategory = (req, res) => {
    commonService.viewItem(categoriesModel, category_model_name, req, res);
}

const editCategory = (req, res) => {
    let body = _.pick(req.body, ['sName'])
    removenull(body)
    commonService.editItem(categoriesModel, 'sName', category_model_name, body, req, res);
}

const deleteCategory = (req, res) => {
    commonService.deleteItem(categoriesModel, category_model_name, req, res);
}

const changeStatus = (req, res) => {
    commonService.changeItemStatus(categoriesModel, category_model_name, req, res);
}

module.exports = {
    addCategory,
    viewCategory,
    editCategory,
    deleteCategory,
    changeStatus,
    list
}
