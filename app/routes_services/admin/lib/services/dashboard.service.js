const { _ } = require('@utils/lib/helpers');
const { removenull } = require('@utils/lib/helpers');
const { messages, endpoints } = require('@utils')
const commonService = require('./common.service');
const customerModel = require('@models/customer.model');
const customer_model_name = 'Customer'

const showDashboardData = (req, res) => {
    let filterOption = req.body.sFilterType
    let customerFilterQuery = [{}]
    let currentDate = new Date()
    var d = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate(), 0, 0, 0);
    if (filterOption === 'daily')
        customerFilterQuery = [{ $match: { dCreatedAt: { $gte: d } } }]
    if (filterOption === 'weekly') {
        var first = currentDate.getDate() - currentDate.getDay();
        var last = first + 6;
        var firstday = new Date(currentDate.setDate(first));
        var lastday = new Date(currentDate.setDate(last));
        customerFilterQuery = [{ $match: { $and: [{ dCreatedAt: { $gte: firstday } }, { dCreatedAt: { $lte: lastday } }] } }]
    }
    if (filterOption === 'monthly') {
        let currentMonth = currentDate.getMonth() + 1
        customerFilterQuery = [{ $project: { month: { $month: '$dCreatedAt' } } }, { $match: { month: { $eq: currentMonth } } }]
    }
    if (filterOption === 'yearly') {
        let currentYear = currentDate.getFullYear()
        customerFilterQuery = [{ $project: { year: { $year: '$dCreatedAt' } } }, { $match: { year: { $eq: currentYear } } }]
    }
    customerModel.aggregate(customerFilterQuery, (error, docs) => {
        if (error) {
            console.log("error :: ", error);
            return endpoints.send(res, messages.custom('FETCH_ERR'))
        }
        return endpoints.send(res, messages.success(), { customerCount: docs.length })
    });
}

module.exports = {
    showDashboardData
}