const { messages, endpoints } = require('@utils')
const { _ } = require('@utils/lib/helpers');
const jwt = require('jsonwebtoken');
const { sendmail } = require('@utils/lib/services/nodemailer');
const { removenull } = require('@utils/lib/helpers');
// const usersModel = require('@models/users.model');
const customerModel = require('@models/customer.model');
const commonService = require('./common.service');
const ObjectId = require('mongodb').ObjectID;
const customer_model_name = 'Customer'
const debug = require('debug')('http')

const createCustomer = (req, res) => {
    try {
        let body = _.pick(req.body, ['sFullName', 'sEmail', 'sPassword', 'sPhoneNumber', 'sCountry', 'sCurrency', 'sAddress']);
        removenull(body)
        if (body.sFullName) {
            var splitsFullName = body.sFullName.toLowerCase().split(' ')
            for (let i = 0; i < splitsFullName.length; i++) {
                splitsFullName[i] = splitsFullName[i].charAt(0).toUpperCase() + splitsFullName[i].substring(1)
            }
            body.sFullName = splitsFullName.join(' ')
        }
        if (body.sPhoneNumber) {
            var query = {
                $or: [{ sEmail: body.sEmail }, { sPhoneNumber: body.sPhoneNumber }]
            }
        } else {
            var query = {
                sEmail: body.sEmail
            }
        }
        customerModel.findOne(query).then((user_found) => {
            if (user_found) {
                if (body.sPhoneNumber && body.sPhoneNumber === user_found.sPhoneNumber) return endpoints.send(res, messages.already_exists('PhoneNumber'))
                if (body.sEmail && body.sEmail === user_found.sEmail) return endpoints.send(res, messages.already_exists('Email'))
            }
            let customer = new customerModel(body)
            customer.save().then((customer) => {
                return endpoints.send(res, messages.created(customer_model_name), customer)
            }).catch((error) => {
                debug('error :: ', error)
                return endpoints.send(res, messages.custom('CUSTOMER_SAVE_ERR'))
            })
        }).catch((error) => {
            debug('error :: ', error)
            return endpoints.send(res, messages.custom('FETCH_ERR'))
        })
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

const customerList = (req, res) => {
    try {
        if (!req.body.pageNumber) {
            req.body.pageNumber = 0
        }
        let search = ''
        if (req.body.search && req.body.search != '') {
            search = req.body.search
                .replace(/\\/g, '\\\\')
                .replace(/\$/g, '\\$')
                .replace(/\*/g, '\\*')
                .replace(/\)/g, '\\)')
                .replace(/\(/g, '\\(')
                .replace(/'/g, "\\'")
                .replace(/"/g, '\\"').trim();
        }
        let sort;
        if (req.body.sortby && req.body.sortby != '') {
            let stype = req.body.sortby;
            let sdir = (req.body.orderby === 'desc') ? -1 : 1;
            sort = {
                [stype]: sdir
            }
        } else {
            sort = {
                'dCreatedAt': -1
            }
        }
        var and = [{ bUserStatus: true }]
        let query = {
            $and: and,
            $or: [
                // {
                //     sFirstName: { $regex: new RegExp('^.*' + search + '.*', 'i') }
                // },
                // {
                //     sLastName: { $regex: new RegExp('^.*' + search + '.*', 'i') }
                // },
                {
                    sEmail: { $regex: new RegExp('^.*' + search + '.*', 'i') }
                },
                {
                    sFullName: { $regex: new RegExp('^.*' + search + '.*', 'i') }
                },
                {
                    sPhoneNumber: { $regex: '^' + search, $options: 'i' }
                }
            ]
        }
        let aggrigationQuery = [
            {
                $addFields: {
                    "sFullName": { $concat: ["$sFirstName", " ", "$sLastName"] }
                }
            },
            {
                $facet: {
                    "data": [
                        {
                            $match: query
                        },
                        {
                            $sort: sort
                        },
                        {
                            $skip: parseInt(req.body.start)
                        },
                        {
                            $limit: parseInt(req.body.size)
                        },
                        {
                            $project: {
                                sFullName: 1,
                                sEmail: 1,
                                sPhoneNumber: { $ifNull: ["$sPhoneNumber", "-"] },
                                eIsActive: 1
                            }
                        }
                    ],
                    "count": [{
                        $match: query
                    }, {
                        $count: "totalData"
                    }]
                }
            }]

        customerModel.aggregate(aggrigationQuery, (error, docs) => {
            if (error) {
                console.log("error :: ", error);

                return endpoints.send(res, messages.custom('FETCH_ERR'))
            }
            if (docs[0].data.length <= 0) docs[0].count[0] = { "totalData": 0 }
            let result = {
                data: docs[0].data,
                count: docs[0].count[0]
            }
            if (docs.length) return endpoints.send(res, messages.list_succ(customer_model_name), result)
            return endpoints.send(res, messages.list_succ(customer_model_name), result)
        });
    } catch (error) {
        debug('error :: ', error)
        return endpoints.send(res, messages.server_error())
    }
}

const viewCustomer = (req, res) => {
    customerModel.findById(req.params.iItemId).then((customerFound) => {
        if (!customerFound) return endpoints.send(res, messages.not_found(customer_model_name))
        customerModel.filterUserData(customerFound)
        return endpoints.send(res, messages.success(), customerFound)
    }).catch((error) => {
        console.log("error :: ", error);
        return endpoints.send(res, messages.custom('FETCH_ERR'))
    })
}

const deleteCustomer = (req, res) => {
    customerModel.findByIdAndUpdate(ObjectId(req.params.iItemId), { $set: { bUserStatus: false } }, { new: true }).then((item) => {
        if (!item) return endpoints.send(res, messages.not_found(customer_model_name))
        return endpoints.send(res, messages.delete_succ(customer_model_name), item)
    }).catch((error) => {
        debug('error :: ', error)
        return endpoints.send(res, messages.custom('REMOVE_ERR'))
    })
}

const changeStatus = (req, res) => {
    let body = {
        eIsActive: req.body.eIsActive
    }
    customerModel.findByIdAndUpdate(req.body.iItemId, { $set: body }, { new: true }).then((updatedCustomer) => {
        if (!updatedCustomer) return endpoints.send(res, messages.not_found(customer_model_name))
        return endpoints.send(res, messages.updated(customer_model_name), updatedCustomer)
    }).catch((error) => {
        debug('error :: ', error)
        return endpoints.send(res, messages.catch('FETCH_ERR'))
    })
}



module.exports = {
    createCustomer,
    viewCustomer,
    deleteCustomer,
    changeStatus,
    customerList
}
