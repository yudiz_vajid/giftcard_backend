const { _ } = require('@utils/lib/helpers');
const { endpoints, messages } = require('@utils')
const { removenull } = require('@utils/lib/helpers');
const loyaltyModel = require('@models/loyalty.model')
const cardModel = require('@models/giftcard.model')
const commonService = require('./common.service');
const loyalty_model_name = 'Loyalty'
const giftCard_model_name = 'Gift Card'

const addLoyalty = (req, res) => {
    let body = _.pick(req.body, ['iCardId', 'nLoyaltyPoint'])
    cardModel.findById(body.iCardId).then((cardFound) => {
        if (!cardFound) return endpoints.send(res, messages.not_found(giftCard_model_name))
        let loyalty = new loyaltyModel({ iCardId: body.iCardId, nLoyaltyPoint: body.nLoyaltyPoint })
        loyalty.save().then((savedLoyalty) => {
            return endpoints.send(res, messages.added(loyalty_model_name), savedLoyalty)
        }).catch((error) => {
            console.log("error :: ", error);
            return endpoints.send(res, messages.server_error())
        })
    }).catch((error) => {
        console.log("error :: ", error);
        return endpoints.send(res, messages.server_error())
    })
}

const list = (req, res, next) => {
    try {
        if (!req.body.start) {
            req.body.start = 0
        }
        let search = ''
        if (req.body.search && req.body.search != '') {
            search = req.body.search
                .replace(/\\/g, '\\\\')
                .replace(/\$/g, '\\$')
                .replace(/\*/g, '\\*')
                .replace(/\)/g, '\\)')
                .replace(/\(/g, '\\(')
                .replace(/'/g, "\\'")
                .replace(/"/g, '\\"').trim();
        }

        let sort;
        if (req.body.sortby && req.body.sortby != '') {
            let stype = req.body.sortby;
            let sdir = (req.body.orderby === 'desc') ? -1 : 1;
            sort = {
                [stype]: sdir
            }
        } else {
            sort = {
                'dCreatedAt': -1
            }
        }
        let aggrigationQuery = [{
            $facet: {
                "data": [
                    {
                        $lookup: {
                            from: 'giftcards',
                            localField: 'iCardId',
                            foreignField: '_id',
                            as: 'product_data'
                        }
                    },
                    {
                        $unwind: {
                            path: '$product_data',
                            preserveNullAndEmptyArrays: true
                        }
                    },
                    {
                        $project: {
                            "productName": "$product_data.sTitle",
                            "Price": "$product_data.nPrice",
                            "Rewards": "$nLoyaltyPoint",
                            "eIsActive": "$eIsActive"
                        }
                    },
                    {
                        $sort: sort
                    },
                    {
                        $skip: parseInt(req.body.start)
                    },
                    {
                        $limit: parseInt(req.body.size)
                    }

                ],
                "count": [{
                    $count: "totalData"
                }]
            }
        }]

        loyaltyModel.aggregate(aggrigationQuery, (error, docs) => {
            if (error) return endpoints.send(res, messages.custom('FETCH_ERR'))
            if (docs[0].data.length <= 0) docs[0].count[0] = { "totalData": 0 }
            docs[0].data.map(singleItem => {
                if (singleItem.iCityId) {
                    var cityName = cities.filter(function (item) {
                        return item.id == singleItem.iCityId
                    });
                    singleItem.sCityName = cityName[0].name
                }
            })
            let result = {
                data: docs[0].data,
                count: docs[0].count[0]
            }
            return endpoints.send(res, messages.list_succ(loyalty_model_name), result)
        });
    } catch (error) {
        console.log("error :: ", error);
        return endpoints.send(res, messages.server_error())
    }

}

const editLoyalty = (req, res) => {
    let body = _.pick(req.body, ['iCardId', 'nLoyaltyPoint'])
    loyaltyModel.findById(req.params.iItemId).then((loyaltyFound) => {
        if (!loyaltyFound) return endpoints.send(res, messages.not_found(loyalty_model_name))
        cardModel.findById(body.iCardId).then((cardFound) => {
            if (!cardFound) return endpoints.send(res, messages.not_found(giftCard_model_name))
            loyaltyFound.iCardId = body.iCardId
            loyaltyFound.nLoyaltyPoint = body.nLoyaltyPoint
            loyaltyFound.save().then((savedLoyalty) => {
                return endpoints.send(res, messages.updated_succ(loyalty_model_name), savedLoyalty)
            }).catch((error) => {
                console.log("error :: ", error);
                return endpoints.send(res, messages.server_error())
            })
        }).catch((error) => {
            console.log("error :: ", error);
            return endpoints.send(res, messages.server_error())
        })
    }).catch((error) => {
        console.log("error :: ", error);
        return endpoints.send(res, messages.server_error())
    })
}

const deleteLoyalty = (req, res) => {
    commonService.deleteItem(loyaltyModel, loyalty_model_name, req, res);
}

const changeStatus = (req, res) => {
    commonService.changeItemStatus(loyaltyModel, loyalty_model_name, req, res);
}

module.exports = {
    addLoyalty,
    editLoyalty,
    deleteLoyalty,
    changeStatus,
    list
}