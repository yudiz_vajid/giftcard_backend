const { _ } = require('@utils/lib/helpers');
const { removenull } = require('@utils/lib/helpers');
const cmsModel = require('@models/cms.model')
const commonService = require('./common.service');
const Cms_model_name = 'Cms'

const addCms = (req, res) => {
    let body = _.pick(req.body, ['sPageName', 'sDesc'])
    removenull(body)
    commonService.addItem(cmsModel, 'sPageName', Cms_model_name, body, req, res);
}


const list = (req, res, next) => {
    commonService.listDynamicItem(cmsModel, Cms_model_name, req, res);
}

const deatailCms = (req, res) => {
    commonService.viewItem(cmsModel, Cms_model_name, req, res);
}

const editCms = (req, res) => {
    let body = _.pick(req.body, ['sPageName', 'sDesc'])
    removenull(body)
    commonService.editItem(cmsModel, 'sPageName', Cms_model_name, body, req, res);
}

const deleteCms = (req, res) => {
    commonService.deleteItem(cmsModel, Cms_model_name, req, res);
}

const changeStatus = (req, res) => {
    commonService.changeItemStatus(cmsModel, Cms_model_name, req, res);
}

module.exports = {
    addCms,
    deatailCms,
    editCms,
    deleteCms,
    changeStatus,
    list
}