const AuthRouter = require('express').Router()

const commonValidator = require('@userLib/validators')
const validator = require('./lib/validators')
const middlewares = require('./lib/middlewares')
const controllers = require('./lib/controllers')
const userControllers = require('@userLib/controllers')
const subAdminService = require('./lib/services/subAdmin.service')
const cmsService = require('./lib/services/cms.service')
const customerService = require('./lib/services/customer.service')
const giftCardService = require('./lib/services/giftCard.service')
const categoryService = require('./lib/services/category.service')
const loyaltyService = require('./lib/services/loyalty.service')
const faqService = require('./lib/services/faq.service')
const dashboardService = require('./lib/services/dashboard.service')

// IMAGE SERVICE 
AuthRouter.get('/imageresize/sharpresize', controllers.sharpimage);

// ADMIN AUTH
AuthRouter.post('/login', commonValidator.login, controllers.adminLogin)
AuthRouter.get('/logout', middlewares.isUserAuthenticated, userControllers.userLogout)
AuthRouter.post('/forgot/password/mail', commonValidator.forgotPasswordmail, controllers.adminForgotPasswordMail)
AuthRouter.post('/reset/password', commonValidator.forgotPassword, controllers.adminForgotPassword);
AuthRouter.get('/verifytoken/:sVerificationToken', controllers.verifyToken)
AuthRouter.get('/profile', middlewares.isUserAuthenticated, controllers.getProfile)
AuthRouter.put('/profile', validator.updateProfile, middlewares.isUserAuthenticated, controllers.updateProfile)
AuthRouter.post('/password/change', commonValidator.changePassword, middlewares.isUserAuthenticated, controllers.adminChangePassword);
AuthRouter.post('/profilepicture', middlewares.isUserAuthenticated, controllers.adminProfilePictureUpload);
AuthRouter.get('/token/verify', middlewares.isUserAuthenticated, controllers.verifiedAdmin)
// AuthRouter.post('/commonuserdata', controllers.getCommonUserData)


// SubAdmin Management
AuthRouter.post('/subadmin', middlewares.isUserAuthenticated, validator.addSubAdmin, subAdminService.createSubAdmin)
AuthRouter.post('/subadmin/list', middlewares.isUserAuthenticated, validator.list, subAdminService.subAdminList)
AuthRouter.get('/subadmin/:iItemId', middlewares.isUserAuthenticated, subAdminService.viewSubAdmin)
AuthRouter.delete('/subadmin/:iItemId', middlewares.isUserAuthenticated, subAdminService.deleteSubAdmin)
AuthRouter.post('/subadmin/changestatus', middlewares.isUserAuthenticated, subAdminService.changeStatus)
AuthRouter.put('/subadmin/:iItemId', middlewares.isUserAuthenticated, subAdminService.editSubAdmin)
AuthRouter.get('/subadmin/mail/verification/:sVerificationToken', subAdminService.verifyMail)



/**
 * Customer service
 */
AuthRouter.post('/customer', middlewares.isUserAuthenticated, customerService.createCustomer)
AuthRouter.get('/customer/:iItemId', middlewares.isUserAuthenticated, customerService.viewCustomer)
AuthRouter.delete('/customer/:iItemId', middlewares.isUserAuthenticated, customerService.deleteCustomer)
AuthRouter.post('/customer/changestatus', middlewares.isUserAuthenticated, customerService.changeStatus)
AuthRouter.post('/customer/list', middlewares.isUserAuthenticated, customerService.customerList)


/**
 * GiftCard manage 
 */
AuthRouter.post('/giftcard', middlewares.isUserAuthenticated, giftCardService.addCard)
AuthRouter.put('/giftcard/:iItemId', middlewares.isUserAuthenticated, giftCardService.editCard)
AuthRouter.get('/giftcard/:iItemId', middlewares.isUserAuthenticated, giftCardService.viewCard)
AuthRouter.delete('/giftcard/:iItemId', middlewares.isUserAuthenticated, giftCardService.deleteCard)
AuthRouter.post('/giftcard/changestatus', middlewares.isUserAuthenticated, giftCardService.changeStatus)
AuthRouter.post('/giftCard/list', middlewares.isUserAuthenticated, giftCardService.list)
AuthRouter.get('/simpleCard/list', middlewares.isUserAuthenticated, giftCardService.simpleSingleList)
AuthRouter.post('/giftCard/list/name', middlewares.isUserAuthenticated, giftCardService.getCardListByName)

/**
 * Categories
 */
AuthRouter.post('/category', middlewares.isUserAuthenticated, categoryService.addCategory)
AuthRouter.get('/category/:iItemId', middlewares.isUserAuthenticated, categoryService.viewCategory)
AuthRouter.put('/category/:iItemId', middlewares.isUserAuthenticated, categoryService.editCategory)
AuthRouter.delete('/category/:iItemId', middlewares.isUserAuthenticated, categoryService.deleteCategory)
AuthRouter.post('/category/changestatus', middlewares.isUserAuthenticated, categoryService.changeStatus)
AuthRouter.post('/category/list', middlewares.isUserAuthenticated, categoryService.list)

/**
 * Loyalty Routes
 */
AuthRouter.post('/loyalty', middlewares.isUserAuthenticated, loyaltyService.addLoyalty)
AuthRouter.put('/loyalty/:iItemId', middlewares.isUserAuthenticated, loyaltyService.editLoyalty)
AuthRouter.delete('/loyalty/:iItemId', middlewares.isUserAuthenticated, loyaltyService.deleteLoyalty)
AuthRouter.post('/loyalty/changestatus', middlewares.isUserAuthenticated, loyaltyService.changeStatus)
AuthRouter.post('/loyalty/list', middlewares.isUserAuthenticated, loyaltyService.list)


// cms Management
AuthRouter.post('/cms', middlewares.isUserAuthenticated, validator.addCms, cmsService.addCms)
AuthRouter.get('/cms/:iItemId', middlewares.isUserAuthenticated, cmsService.deatailCms)
AuthRouter.put('/cms/:iItemId', middlewares.isUserAuthenticated, cmsService.editCms)
AuthRouter.post('/list/cms', middlewares.isUserAuthenticated, validator.list, cmsService.list)
AuthRouter.post('/cms/changestatus', middlewares.isUserAuthenticated, cmsService.changeStatus)
AuthRouter.delete('/cms/:iItemId', middlewares.isUserAuthenticated, cmsService.deleteCms)

/**
 * Question service
 */
AuthRouter.post('/faq', validator.faq, middlewares.isUserAuthenticated, faqService.createQuestion)

/**
 * Dashboard service
 */
AuthRouter.post('/dashboard', middlewares.isUserAuthenticated, dashboardService.showDashboardData)


module.exports = AuthRouter