/**
 * NOTE : Make sure jimpImagelocal service store thumbnail image in local storage & return only new image path 
 * Sharp will change image quality at runtime with h(hight),w(width),q(quality)
 */
const fs = require('fs');
const sharp = require('sharp');
var Jimp = require("jimp");
const debug = require('debug')('http')

const jimpImagelocal = (data, callback) => {
    var imageName = data.src.split(/[/ ]+/).pop()
    var w = parseInt(data.w), h = parseInt(data.h), q = parseInt(data.q);
    Jimp.read(data.src)
        .then(image => {
            let thumbnailPath = `public/uploads/thumbnail/${Date.now()}${imageName}`;
            image.resize(h, w); // resize
            image.quality(q) // set JPEG quality
            image.write(thumbnailPath, function () {    //save new thumb image in local
                return callback(null, '/' + thumbnailPath)
            })
        })
        .catch(err => {
            console.error(err);
        });
}

const jimpImage = (req, res, next) => {
    var data = req.query
    var w = parseInt(data.w), h = parseInt(data.h), q = parseInt(data.q);
    Jimp.read(data.src)
        .then(image => {
            image.quality(q)
            image.resize(parseInt(h), parseInt(w)).getBuffer(Jimp.MIME_JPEG, function (err, buffer) {
                res.set("Content-Type", Jimp.MIME_JPEG);
                res.send(buffer);
            });
        })
}

const sharpImage = function resize(path, width, height, quality) {
    try {
        const readStream = fs.createReadStream(path);
        let transform = sharp();
        if (width || height) {
            transform = transform.resize(width, height, { fit: sharp.fit.fill })
        }
        if (quality) {
            transform.jpeg({
                quality: quality
            })
        }
        return readStream.pipe(transform);

    } catch (error) {
        debug("error :: ", error)
    }
};

module.exports = {
    sharpImage,
    jimpImagelocal,
    jimpImage
}
