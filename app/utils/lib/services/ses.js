const AWS = require('aws-sdk')

AWS.config.region = process.env.AWS_REGION
AWS.config.update({
    accessKeyId: process.env.AWS_ACCESSKEYID,
    secretAccessKey: process.env.AWS_SECRETKEY,
})

const ses_client = new AWS.SES()

const services = {}

services.sendEmail = (toEmails, subject, data, callback) => {
    ses_client.sendEmail({
        Source: process.env.SOURCE_EMAIL,
        Destination: {
            ToAddresses: toEmails
        },
        Message: {
            Subject: {
                Data: subject
            },
            Body: {
                Text: {
                    Data: data
                }
            }
        }
    }, callback)
}

module.exports = services

