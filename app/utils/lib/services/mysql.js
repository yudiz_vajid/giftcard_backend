const mysql = require('mysql')
const log = require('./../log')
const _ = require('./../helpers')

const connection = mysql.createConnection({
    host: process.env.MYSQL_HOST,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    port: process.env.MYSQL_PORT,
    database: process.env.MYSQL_DATABASE,
})

const sql = {}

connection.connect((err, success) => {
    if (err) return log.red(err)
    log.green("Database connected ...")
})

const logQuery = (query, params) => {
    log.magenta(mysql.format(query, params))
}

const _query = (query, params, callback) => {
    logQuery(query, params)
    connection.query(query, params, (error, results, fields) => {
        if (error) return callback(error)
        return callback(null, results)
    })
}

sql.select = (query, params, single, callback) => {
    _query(query, params, (error, response) => {
        if (error) return callback(error)
        return callback(null, single ? response[0] : response)
    })
}

sql.insert = (query, params, callback) => {
    params.created_at = _.isoTimestring()
    _query(query, params, callback)
}

sql.update = (query, params, callback) => {
    params[0].updated_at = _.isoTimestring()
    _query(query, params, callback)
}

sql.remove = (query, params, callback) => {
    params[0].deleted_at = _.isoTimestring()
    _query(query, params, callback)
}

sql.hardRemove = (query, params, callback) => {
    _query(query, params, callback)
}

module.exports = sql