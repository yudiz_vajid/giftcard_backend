/**
 * DevelopeBy: Vajid khokhar
*/

const imagemin = require('imagemin');
const fs = require('fs');
const pngToJpeg = require('png-to-jpeg');
const mime = require('mime');
var Jimp = require('jimp');


const compressImage = function (filePath) {
    return new Promise(function (resolve, reject) {
        const imageminMozjpeg = require('imagemin-mozjpeg');
        (async () => {
            const Compressed_image = await imagemin(
                [filePath],
                process.env.sponsorImageCompPath,
                { plugins: [imageminMozjpeg({ quality: 50 })] }
            );
            try {
                let file_exist = fs.statSync(filePath);
                if (file_exist) {
                    fs.unlinkSync(filePath);
                }
            } catch (error) {
                console.log('file does not exist in local');
            }
            resolve(Compressed_image)
        })();
    })

}

const pngTojpg = function (fileObj, img_name, req) {
    return new Promise(async function (resolve, reject) {
        var imgname = `${fileObj.originalname.toString().split(".")[0]}_${req._id}_${Date.now()}.${mime.getExtension(fileObj.mimetype)}`
        fs.writeFileSync(`${process.env.sponsorImagePath}/${imgname}`, fileObj.buffer)
        let newImage = `${process.env.sponsorImagePath}/${img_name}`
        await Jimp.read(process.env.sponsorImagePath + '/' + imgname, (err, lenna) => {
            if (err) reject(err)
            lenna
                .quality(80)
                .write(newImage);
            try {
                const stats = fs.statSync(`${process.env.sponsorImagePath}/${imgname}`);
                if (stats) {
                    fs.unlinkSync(`${process.env.sponsorImagePath}/${imgname}`);
                }
            }
            catch (err) {
                console.log('file does not exist in local');
            }
            resolve(newImage)
        });
    })
}

module.exports = {
    compressImage,
    pngTojpg
}