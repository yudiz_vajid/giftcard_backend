const Nexmo = require('nexmo')
const services = { }

const nexmo = new Nexmo({
    apiKey: process.env.NEXMO_APIKEY,
    apiSecret: process.env.NEXMO_APISECRET
})

services.sendMessage = (from, to, text, callback) => {
    nexmo.message.sendSms(from, to, text, (error, response) => {
        if (error) return callback(error)
        if(response.messages[0].status != '0') return callback('Nexmo returned back a non-zero status')
        callback(null, response)
    })
}

module.exports = services

