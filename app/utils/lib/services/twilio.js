
const client = require('twilio')({
    accountSid: process.env.TWILIO_ACCOUNTSID,
    secret: process.env.TWILIO_SECRET
});

const services = {}

services.sendMessage = (PhoneNumber, Message, callback) => {
    client.messages.create({
        from: '***********',
        to: PhoneNumber,
        body: Message
    }).then((messsage) => callback(null, messsage)).catch(callback)
}

module.exports = services