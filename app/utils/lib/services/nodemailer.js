/**
 * Created By Vajid khokhar
 * 
 * @method {sendmail} is for generating trasport and sending mail with specified mailOptions Object And returns a promise ex: { from:'', to:'',subject: '', html: '' }
 */

const nodemailer = require('nodemailer');
// const config = require('./../../config');
const fs = require('fs');
const ejs = require('ejs');
// const { messages, status, jsonStatus } = require('../../api.response');
// const { messages, endpoints, _, log } = require('../../../utils')
// const errorLogs = fs.createWriteStream('error.log', { flags: 'a' });

const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    auth: {
        user: process.env.SMTP_USER_EMAIL || 'example@gmail.com', // SMTP email
        pass: process.env.SMTP_PASS || 'example@123' // Your password
    },
    secure: true
});


const sendmail = (templateName, data, mailOption) => {
    let template = fs.readFileSync(process.env.EMAIL_TEMPLATE_PATH + templateName, {
        encoding: 'utf-8' //Unicode Transformation Format (UTF).
    });

    let emailBody = ejs.render(template, data);

    mailOption.html = emailBody;

    let nodeMailerOptions = mailOption;
    return transporter.sendMail(nodeMailerOptions);
};



module.exports = {
    sendmail
}