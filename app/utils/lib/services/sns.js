const AWS_SNS = require('aws-sdk/clients/sns')

const sns = new AWS_SNS({
    region: process.env.AWS_REGION,
    accessKeyId: process.env.AWS_ACCESSKEYID,
    secretAccessKey: process.env.AWS_SECRETKEY
})

const services = {}

services.sendMessage = (Message, PhoneNumber, Subject, callback) => {
    const params = {
        Message,
        PhoneNumber,
        Subject,
        MessageStructure: 'String',
        MessageAttributes: {
            'AWS.SNS.SMS.SenderID': {
                DataType: 'String',
                StringValue: '******'
            },
            'AWS.SNS.SMS.SMSType': {
                DataType: 'String',
                StringValue: 'Transactional'
            }
        }
    }
    if(process.env.PROD) sns.publish(params, callback)
    else callback()
}

module.exports = services

/**
 *  Example
 *  Function -> sendMessage('Hello', 'mobilenumber',"Test")
 */