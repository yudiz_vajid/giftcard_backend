const endpoints = {}

endpoints.send = (res, { code, message }, data = {}, header = undefined) => {
    res.status(code).header(header).json({ message, data })
}


module.exports = endpoints