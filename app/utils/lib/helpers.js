const crypto = require('crypto')
const jwt = require('jsonwebtoken')

const _ = {}

const config = {
    UPLOAD_PATH: process.env.UPLOAD_PATH,
    BASE_URL: process.env.BASE_URL,
    PORT: process.env.PORT,
    PROD: process.env.PROD,
    VERIFICATION_CODE_LENGTH: process.env.VERIFICATION_CODE_LENGTH,
    JWT_SECRET: process.env.JWT_SECRET
}

_.parse = function (data) {
    return JSON.parse(data)
}

_.stringify = function (data, offset = 0) {
    return JSON.stringify(data, null, offset)
}

_.clone = function (data) {
    return this.parse(this.stringify(data))
}

_.pick = function (obj, array) {
    return array.reduce((acc, elem) => {
        if (this.clone(obj).hasOwnProperty(elem)) acc[elem] = obj[elem]
        return acc
    }, {})
}

_.ommit = function (obj, array) {
    let clonedObject = this.clone(obj)
    let objectKeys = Object.keys(clonedObject)
    return objectKeys.reduce((acc, elem) => {
        if (!array.includes(elem)) acc[elem] = clonedObject[elem];
        return acc
    }, {})
}

_.uploadPath = function (filename) {
    const pathArray = config.UPLOAD_PATH.split('/')
    const uploadpath = pathArray[pathArray.length - 1]
    return filename ? `http://${config.BASE_URL}:${config.PORT}/${uploadpath}/${filename}` : ''
}

_.isoTimestring = function () {
    let today = new Date()
    return today
}

_.encryptPassword = function (password) {
    return crypto.createHmac('sha256', config.JWT_SECRET).update(password).digest('hex');
}

_.salt = function (length, type) {
    if (type === 'string') {
        return crypto.randomBytes(Math.ceil(length / 2)).toString('hex').slice(0, length)
    } else if (!config.PROD) {
        return '0'.repeat(config.VERIFICATION_CODE_LENGTH)
    } else {

        let min = 1
        let max = 9
        for (let i = 1; i < length; i++) {
            min += '0'
            max += '9'
        }
        min = Math.ceil(min)
        max = Math.floor(max)
        return Math.floor(Math.random() * (max - min + 1)) + min
    }
}

_.encodeToken = function (body) {
    try {
        return jwt.sign(body, config.JWT_SECRET)
    } catch (error) {
        return undefined
    }
}

_.decodeToken = function (token) {
    try {
        return jwt.decode(token, config.JWT_SECRET)
    } catch (error) {
        return undefined
    }
}

_.verifyToken = function (token) {
    return jwt.verify(token, config.JWT_SECRET, function (err, decoded) {
        return err ? err.message : decoded // return true if token expired
    });
}

_.isEmail = function (email) {
    var regeX = /[a-z0-9._%+-]+@[a-z0-9-]+[.]+[a-z]{2,5}$/;
    return !regeX.test(email);
}

_.isFullName = function (name) {
    var regeX = /^[a-zA-Z ]+$/;
    return !regeX.test(name);
}

_.validatePassword = function (password) {
    var regeX = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,15}$/;
    return !regeX.test(password);
}

_.isEmpty = function isEmpty(value) {
    return value === undefined ||
        value === null ||
        (typeof value === "object" && Object.keys(value).length === 0) ||
        (typeof value === "string" && value.trim().length === 0)
}

const removenull = (obj) => {
    for (var propName in obj) {
        if (obj[propName] === null || obj[propName] === undefined || obj[propName] === '') {
            delete obj[propName];
        }
    }
}


module.exports = { _ ,removenull}