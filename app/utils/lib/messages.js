const customMessages = {
    OTP_SENT: [200, 'Otp sent to your registered mobile number'],
    MAIL_SENT_FAIL: [500, 'Mail sent fail on given Email'],
    MAIL_SENT_SUCC: [200, 'Mail sent successfully'],
    FORGOT_MAIL_SENT: [200, 'Password reset link has been sent to your email address'],
    LINK_EXP: [400, 'The password reset link is no longer valid. Please request another password reset email from the login page'],
    VERIFY_EMAIL_LINK_EXP: [400, 'The Email verification link is no longer valid'],
    EMAIL_NOT_RIGHT: [400, 'Mail is not valid'],
    NOT_REG_EMAIL: [500, 'You are not registered with this email'],
    VERIFY_EMAIL: [403, 'Please verify your email'],
    PASSWORD_CHANGED_SUCC: [200, 'Password changed successfully'],
    OLD_NEW_PASS_SAME: [400, 'Old password and New password can not be same'],
    db_users_error: [500, 'Users database error'],
    reg_succ: [200, 'You have successfully completed registration'],
    LOGIN_SUCC: [200, 'You have successfully logged in'],
    LOGIN_FAIL: [404, 'Incorrect email or password'],
    USER_BLOCKED: [401, 'Your account is currently blocked by admin'],
    DATA_SAVE_ERR: [500, 'Error in saving data. please try again'],
    UPDATE_ERR: [500, 'Error in update data. Please try again'],
    REMOVE_ERR: [500, 'Error in remove data. Please try again'],
    FETCH_ERR: [500, 'Error in fetching data. Please try again'],
    STATUS_SAVE_SUCC: [200, 'Status successfully saved'],
    TREATMENT_SAVE_SUCC: [200, 'Treatment successfully saved'],
    PNG_JPG_ERR: [500, 'Error in converting .png to .jpg file'],
    SPO_IMG_REQ: [400, "Sponsor Image must be in  '.png','.jpg','.jpeg' formate"],
    SEARCH_STRING: [400, 'Search value shoul be in String formate'],
    CATEGORY_SAVE_SUCC: [200, 'Category successfully saved'],
    CATEGORY_LIST_GET_SUCC: [200, 'Category list successfully fetched'],
    CATEGORY_FETCH_ERR: [500, 'Error in fetching Category'],
    SUB_ADMIN_SAVE_ERR: [500, 'Error in saving Sub Admin'],
    CUSTOMER_SAVE_ERR: [500, 'Error in saving Customer'],
    CARD_SAVE_ERR: [500, 'Error in saving Card'],
    INV_FETCH_ERR: [500, 'Error in fatching Investigator data']
}

const builder = {
    server_error: (prefix) => builder.prepare(500, prefix, 'server error'),
    unauthorised: (prefix) => builder.prepare(401, prefix, 'unauthorised'),
    inactive: (prefix) => builder.prepare(403, prefix, 'inactive'),
    not_found: (prefix) => builder.prepare(404, prefix, 'not found'),
    not_matched: (prefix) => builder.prepare(406, prefix, 'not matched'),
    already_exists: (prefix) => builder.prepare(409, prefix, 'already exists'),
    required_field: (prefix) => builder.prepare(417, prefix, 'field required'),
    created: (prefix) => builder.prepare(200, prefix, 'successfully created'),
    added: (prefix) => builder.prepare(200, prefix, 'successfully added'),
    updated: (prefix) => builder.prepare(200, prefix, 'successfully updated'),
    deleted: (prefix) => builder.prepare(200, prefix, 'successfully deleted'),
    success: (prefix) => builder.prepare(200, prefix, 'success'),
    incorrect: (prefix) => builder.prepare(400, prefix, 'Incorrect'),
    blocked: (prefix) => builder.prepare(401, prefix, 'Blocked'),
    logout: (prefix) => builder.prepare(200, prefix, 'You have successfully logged out'),
    custom: (key) => builder.prepare(...customMessages[key], ''),
    list_succ: (prefix) => builder.prepare(200, prefix, 'list get successfully'),
    updated_succ: (prefix) => builder.prepare(200, prefix, 'successfully updated'),
    get_succ: (prefix) => builder.prepare(200, prefix, 'get successfully'),
    delete_succ: (prefix) => builder.prepare(200, prefix, 'successfully deleted'),
    fetch_err: (prefix) => builder.prepare(500, prefix, 'fetch error.Please try again')
}

Object.defineProperty(builder, 'prepare', {
    enumerable: false,
    configurable: false,
    writable: false,
    value: (code, prefix, message) => {
        return {
            code,
            message: `${prefix ? prefix + ' ' + message : message}`
        }
    }
});

module.exports = builder

/**
 *   
 * builder.custom holds custom messge key as a prefix and value provided in second params as `postfix` 
 * other then this custom method 
 * all other method has second parameter as `prefix`
 * 
 */