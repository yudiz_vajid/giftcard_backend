module.exports = {
    endpoints: require('./lib/endpoints'),
    messages: require('./lib/messages'),
    log: require('./lib/log'),
    // services: {
    //     sql: require('./lib/services/mysql'),
    //     multer: require('./lib/services/multer'),
    //     sns: require('./lib/services/sns'),
    //     ses: require('./lib/services/ses'),
    //     twilio: require('./lib/services/twilio'),
    //     nexmo: require('./lib/services/nexmo')
    // }
}