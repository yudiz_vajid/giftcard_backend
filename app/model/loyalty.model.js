const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

require('dotenv').config();

const Loyalties = new Schema({
    iCardId: { type: Schema.Types.ObjectId, ref: 'GiftCards' },
    eIsActive: { type: String, enum: ['y', 'n', 'b'], default: 'y' },//y=active, n=email not verified, b=user blocked(in-active)
    nLoyaltyPoint: { type: Number },
    dUpdatedAt: { type: Date },
    dCreatedAt: { type: Date, default: Date.now }
});

Loyalties.statics.filterUserData = function (loyalty) {
    if (loyalty) {
        loyalty.__v = undefined;
        loyalty.dCreatedAt = undefined;
        loyalty.bStatus = undefined;
        return loyalty;
    }
}


module.exports = mongoose.model('Loyalties', Loyalties);