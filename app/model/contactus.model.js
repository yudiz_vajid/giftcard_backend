/*
 * ContactUs model schema
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Contactus = new Schema(
    {
        sFullName: {
            type: String
        },
        sEmail: {
            type: String
        },
        sSubject: {
            type: String
        },
        sMessage: {
            type: String
        },
        dCreatedAt: {
            type: Date,
            default: Date.now
        }
    },
    {
        usePushEach: true
    });

Contactus.pre('save', function (next) {
    var cms = this;
    next();
});

module.exports = mongoose.model('Contactus', Contactus);
