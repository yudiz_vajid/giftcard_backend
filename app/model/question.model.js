/*
 * Question model schema
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Question = new Schema(
    {
        sQuestion: {
            type: String
        },
        sAnswer: {
            type: String
        },
        bStatus: {
            type: Boolean,
            default: true
        },
        dCreatedAt: {
            type: Date,
            default: Date.now
        },
        dUpdatedAt: {
            type: Date,
            default: Date.now
        }
    },
    {
        usePushEach: true
    });

Question.pre('save', function (next) {
    var question = this;
    next();
});

module.exports = mongoose.model('Question', Question);
