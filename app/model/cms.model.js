/*
 * Cms model schema
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Cms = new Schema(
    {
        sPageName: {
            type: String
        },
        sDesc: {
            type: String
        },
        bStatus: {
            type: Boolean,
            default: true
        },
        dCreatedAt: {
            type: Date,
            default: Date.now
        },
        dUpdatedAt: {
            type: Date,
            default: Date.now
        }
    },
    {
        usePushEach: true
    });

Cms.pre('save', function (next) {
    var cms = this;
    next();
});

module.exports = mongoose.model('Cms', Cms);
