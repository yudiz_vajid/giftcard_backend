/*
 * Contract model schema
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Categories = new Schema(
    {
        sName: {
            type: String
        },
        bStatus: {
            type: Boolean,
            default: true
        },
        dCreatedAt: {
            type: Date,
            default: Date.now
        },
        dUpdatedAt: {
            type: Date,
            default: Date.now
        }
    },
    {
        usePushEach: true
    });

Categories.pre('save', function (next) {
    var cms = this;
    next();
});

module.exports = mongoose.model('Categories', Categories);
