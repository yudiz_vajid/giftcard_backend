const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt-nodejs');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

require('dotenv').config();

// Pre Defined Messages for errors
const Customers = new Schema({
    sFirstName: { type: String, trim: true },
    sLastName: { type: String, trim: true },
    // sFullName: { type: String, trim: true },
    sPhoneNumber: { type: String },
    sEmail: { type: String, lowercase: true, trim: true, unique: true },
    sPassword: { type: String },
    sCountry: { type: String },
    sCurrency: { type: String },
    sAddress: { type: String },
    eIsActive: { type: String, enum: ['y', 'n', 'b'], default: 'y' },//y=active, n=email not verified, b=user blocked(in-active)
    bUserStatus: { type: Boolean, default: true }, //softdelete
    sProfilePicture: { type: String },
    aJwtTokens: [{ sToken: String, sPushToken: String, sIpAddress: String }],
    sVerificationToken: { type: String },
    sGoogleId: { type: String },
    sVerificationToken: { type: String },
    dUpdatedAt: { type: Date },
    dCreatedAt: { type: Date, default: Date.now },
    bNotificationStatus: { type: Boolean, default: true }
});

/* pre-save before save if the values include password then encode it using bcrypt. */
Customers.pre('save', function (next) {
    let user = this;
    if (user.isModified('sPassword')) {
        user.sPassword = bcrypt.hashSync(user.sPassword, null, null);
    }
    if (user.isModified('sFirstName')) {
        let sFirstName = user.sFirstName
        var splitsFirstName = sFirstName.toLowerCase().split(' ')
        for (let i = 0; i < splitsFirstName.length; i++) {
            splitsFirstName[i] = splitsFirstName[i].charAt(0).toUpperCase() + splitsFirstName[i].substring(1)
        }
        user.sFirstName = splitsFirstName.join(' ')
    }
    if (user.isModified('sLastName')) {
        let sLastName = user.sLastName
        var splitsLastName = sLastName.toLowerCase().split(' ')
        for (let i = 0; i < splitsLastName.length; i++) {
            splitsLastName[i] = splitsLastName[i].charAt(0).toUpperCase() + splitsLastName[i].substring(1)
        }
        user.sLastName = splitsLastName.join(' ')
    }
    next();
});

Customers.statics.filterUserData = function (user) {
    if (user) {
        user.__v = undefined;
        user.sPassword = undefined;
        user.sVerificationToken = undefined;
        user.aJwtTokens = undefined;
        user.dCreatedAt = undefined;
        user.eIsActive = undefined;
        user.bUserStatus = undefined
        user.bNotificationStatus = undefined
        return user;
    }
}

Customers.methods.generateAuthToken = function (req) {
    let user = this;
    let obj = {
        sToken: jwt.sign({
            _id: (user._id).toHexString()
        }, process.env.JWTPASSPHERE)
    };

    user.aJwtTokens.push(obj);
    return user.save().then(function () {
        return obj.sToken;
    }).catch((error) => {
        console.log(error);
    });
}


/* Find user by token */
Customers.statics.findByToken = function (token) {
    let Users = this,
        decoded;
    try {
        decoded = jwt.verify(token, process.env.JWTPASSPHERE);
    } catch (e) {
        return new Promise((resolve, reject) => {
            reject();
        });
    }
    let query = {
        '_id': decoded._id,
        'aJwtTokens.sToken': token
    };
    return Users.findOne(query)
}
Customers.statics.createVerificationToken = function (user_id) {
    return jwt.sign({ _id: (user_id).toHexString() }, process.env.JWT_SECRET, { expiresIn: process.env.JWT_MAIL_VALIDITY });
}

module.exports = mongoose.model('Customers', Customers);