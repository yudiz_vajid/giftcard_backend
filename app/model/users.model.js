const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt-nodejs');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

require('dotenv').config();

// Pre Defined Messages for errors
const Users = new Schema({
    sFirstName: { type: String, trim: true },
    sLastName: { type: String, trim: true },
    sPhoneNumber: { type: String },
    sEmail: { type: String, lowercase: true, trim: true, unique: true },
    sPassword: { type: String },
    eUserType: { type: String, enum: ['user', 'admin'], default: 'user' },
    eIsActive: { type: String, enum: ['y', 'n', 'b'], default: 'n' },//y=active, n=email not verified, b=user blocked(in-active)
    bUserStatus: { type: Boolean, default: true }, //softdelete
    sProfilePicture: { type: String },
    aJwtTokens: [{ sToken: String, sPushToken: String, sIpAddress: String }],
    sVerificationToken: { type: String },
    dUpdatedAt: { type: Date },
    dCreatedAt: { type: Date, default: Date.now },
    bNotificationStatus: { type: Boolean, default: true },
    aPermissions: [{}]
});

/* pre-save before save if the values include password then encode it using bcrypt. */
Users.pre('save', function (next) {
    let user = this;
    if (user.isModified('sPassword')) {
        user.sPassword = bcrypt.hashSync(user.sPassword, null, null);
    }
    next();
});

/* Find user by token */
Users.statics.findByToken = function (token) {
    let Users = this,
        decoded;
    try {
        decoded = jwt.verify(token, process.env.JWTPASSPHERE);
    } catch (e) {
        return new Promise((resolve, reject) => {
            reject();
        });
    }
    let query = {
        '_id': decoded._id,
        'aJwtTokens.sToken': token
    };
    return Users.findOne(query)
}

/* Generate token for login */
Users.methods.generateAuthToken = function (req) {
    let user = this;
    let obj = {
        sToken: jwt.sign({
            _id: (user._id).toHexString()
        }, process.env.JWTPASSPHERE)
    };

    user.aJwtTokens.push(obj);
    return user.save().then(function () {
        return obj.sToken;
    }).catch((error) => {
        console.log(error);
    });
}

Users.statics.filterUserData = function (user) {
    if (user) {
        user.__v = undefined;
        user.sPassword = undefined;
        user.sVerificationToken = undefined;
        user.aJwtTokens = undefined;
        user.sToken = undefined;
        user.dCreatedAt = undefined;
        user.eIsActive = undefined;
        user.bUserStatus = undefined;
        user.eUserType = undefined;
        return user;
    }
}

Users.statics.createVerificationToken = function (user_id) {
    return jwt.sign({ _id: (user_id).toHexString() }, process.env.JWT_SECRET, { expiresIn: process.env.JWT_MAIL_VALIDITY });
}

module.exports = mongoose.model('Users', Users);
// test remove bad commit