const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt-nodejs');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

require('dotenv').config();

// Pre Defined Messages for errors
const GiftCards = new Schema({
    iCategoryId: { type: Schema.Types.ObjectId, ref: 'Categories' },
    sTitle: { type: String, trim: true },
    sDate: { type: String },
    sTime: { type: String },
    sValidity: { type: String },
    nQuantity: { type: Number },
    nOffer: { type: Number },
    sOfferValidity: { type: String },
    nPrice: { type: Number },
    sDescription: { type: String },
    eIsActive: { type: String, enum: ['y', 'n', 'b'], default: 'y' },//y=active, n=email not verified, b=user blocked(in-active)
    bCardStatus: { type: Boolean, default: true }, //softdelete
    sCardImage: { type: String },
    dUpdatedAt: { type: Date },
    dCreatedAt: { type: Date, default: Date.now }
});

GiftCards.statics.filterUserData = function (card) {
    if (card) {
        card.__v = undefined;
        card.dCreatedAt = undefined;
        card.eIsActive = undefined;
        card.bCardStatus = undefined;
        return card;
    }
}


module.exports = mongoose.model('GiftCards', GiftCards);