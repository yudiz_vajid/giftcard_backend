const app = new require('express')()
const mongoose = require('mongoose');
const server = app.listen()

/**
* mongoose connection (url from config/env)
*/
mongoose
    .connect(process.env.DB_URL, { promiseLibrary: global.Promise, useNewUrlParser: true })
    .then(() => console.log('Connected to database..'))
    .catch((error) => console.log("Connection to DB fail --->", error));
mongoose.Promise = global.Promise;
// mongoose.set('debug', true);
mongoose.set('useFindAndModify', false);


require('./app/routes_services')(app)

process.on('message', function (message, connection) {
    if (message !== 'sticky-session:connection') return
    server.emit('connection', connection)
    connection.resume()
})